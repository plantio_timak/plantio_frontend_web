export const COLOR_SECONDARY = "#182628";
export const COLOR_SECONDARY_OPACITY = "rgba(24, 38, 40, 0.75)";
export const COLOR_PRIMARY_LIGHTER = "#b2e5db";
export const COLOR_PRIMARY_LIGHT = "#8bd8c9";
export const COLOR_PRIMARY = "#65ccb8";
export const COLOR_PRIMARY_DARK = "#57ba98";
export const COLOR_PRIMARY_DARKER = "#3b945e";
export const COLOR_BASE = "#f2f2f2";
export const COLOR_ERROR = "#c62828";
