import axios, { AxiosInstance } from "axios";

import { getRealTimeDataApiPath } from "./apiPaths";

const BASE_URL = "147.175.121.239";

const HTTP_PROTOCOL = "http";

const PORT = 8081;

const HTTP_URL = `${HTTP_PROTOCOL}://${BASE_URL}:${PORT}/api`;

const getApiHeaders = (token: string | null) => {
  if (token) {
    return {
      Authorization: token,
      "Content-Type": "application/json",
    };
  }

  return {
    "Content-Type": "application/json",
  };
};

export const api = (token: string | null = ""): AxiosInstance => {
  const headers = getApiHeaders(token);

  return axios.create({
    baseURL: HTTP_URL,
    headers,
  });
};

export const eventSourceApi = (): EventSource =>
  new EventSource(`${HTTP_URL}${getRealTimeDataApiPath()}`);
