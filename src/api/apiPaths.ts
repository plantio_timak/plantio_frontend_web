import { PLANTS_MODULE } from "../constants/Services";
import { PlantsModuleDataDuration } from "../types/PlantsModule";

export const getRegisterUserApiPath = (): string => "/login/registration";
export const getLoginUserApiPath = (): string => "/login/authorization";

export const getRoomDataApiPath = (
  roomId: number,
  duration: PlantsModuleDataDuration
): string =>
  `/watering/my/room/${roomId}?serviceType=${PLANTS_MODULE}&interval=${duration}`;
export const getRoomsApiPath = (): string =>
  `/watering/my/rooms/?serviceType=${PLANTS_MODULE}`;
export const getRoomPlantsDataApiPath = (
  roomId: number,
  duration: PlantsModuleDataDuration
): string => `/watering/my/plants/room/${roomId}?interval=${duration}`;
export const getRoomAddToUserApiPath = (): string => "/watering/my/room/add";
export const getSetLastUsedInstanceApiPath = (): string =>
  "/watering/my/rooms/last-used/set";
export const getCreateRoomForUserApiPath = (): string => "/admin/room/add";
export const getPlantDataEditApiPath = (plantId: number): string =>
  `watering/my/plant/${plantId}`;
export const getPlantModeEditApiPath = (plantId: number): string =>
  `watering/my/plant/${plantId}/plant-mode`;
export const getWateringModesApiPath = (): string => "/watering/watering-modes";
export const getRealTimeDataApiPath = (): string => "/real-time/";
