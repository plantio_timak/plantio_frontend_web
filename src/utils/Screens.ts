export const getPlantDetailsRoutePath = (id: number): string => `/plant/${id}`;
