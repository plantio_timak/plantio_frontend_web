import { PLANTS_MODULE } from "../constants/Services";
import { Service } from "../types/Services";

export const getNavigationModuleLabel = (module: string): string => {
  switch (module) {
    case PLANTS_MODULE:
      return "plants module";
    default:
      return "";
  }
};

export const getNavigationItemValue = (item: string): Service | null => {
  switch (item) {
    case "plants module":
      return PLANTS_MODULE;
    default:
      return null;
  }
};

export const getNavigationItems = (modules: Array<Service>): Array<string> =>
  modules?.map(getNavigationModuleLabel);
