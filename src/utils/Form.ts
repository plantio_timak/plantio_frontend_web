export const onChangeHandler = <T>(
  setValue: (id: T, value: string, conf: { shouldValidate: boolean }) => void
) => (name: unknown, value: string): void =>
  setValue(name as T, value, { shouldValidate: true });

export const onSelectChangeHander = <T>(
  setValue: (
    id: T,
    value: string | number,
    conf: { shouldValidate: boolean }
  ) => void,
  id: T
) => (value: string | number): void => {
  setValue(id, value, { shouldValidate: true });
};
