import {
  PLANTS_MODULE_DURATION_DAY,
  PLANTS_MODULE_DURATION_MONTH,
  PLANTS_MODULE_DURATION_WEEK,
  PLANT_MODE_INTERACTIVE,
  PLANT_MODE_TIME,
  WATERING_MODES_VALUES_LIST,
} from "../constants/PlantsModule";
import {
  PlantData,
  PlantDataApi,
  PlantMode,
  PlantsModuleDataDuration,
  RoomDataApi,
  WateringMode,
} from "../types/PlantsModule";

export const getWateringModeLabel = (mode: PlantMode): string => {
  switch (mode) {
    case PLANT_MODE_INTERACTIVE:
      return "Adaptive";
    case PLANT_MODE_TIME:
      return "Time";
    default:
      return "";
  }
};

export const getUpdatedPlants = (
  plants: Array<PlantData>,
  editedPlant: PlantData
): Array<PlantData> => {
  return plants.map((plant) => {
    if (plant.id === editedPlant?.id) {
      return editedPlant;
    }

    return plant;
  });
};

export const updateRoomValues = (
  values: Array<RoomDataApi> | undefined,
  newValue: RoomDataApi
): Array<RoomDataApi> => {
  if (values) {
    return [newValue, ...values];
  }

  return [newValue];
};

export const updatePlantValues = (
  plants: Array<PlantData> | undefined,
  newValue: PlantDataApi,
  plantId: number
): Array<PlantData> => {
  const plantIndexToUpdate = plants?.findIndex((plant) => plant.id === plantId);

  if (plants && plantIndexToUpdate && plantIndexToUpdate !== -1) {
    const values = plants[plantIndexToUpdate].plantValues;

    const updatedValues = [newValue, ...values];

    plants[plantIndexToUpdate].plantValues == updatedValues;

    return plants;
  }

  return plants || [];
};

export const getLastPlantValueProperty = (
  plantValues: Array<PlantDataApi> | undefined,
  property: "temperature" | "humidity"
): string => {
  const [lastValue] = plantValues || [];

  return lastValue?.values[property].toFixed(2);
};

export const formatLineChartXTick = (
  date: Date,
  duration: PlantsModuleDataDuration
): string => {
  switch (duration) {
    case PLANTS_MODULE_DURATION_DAY: {
      const getHours = () => {
        const dateToUse = new Date(date);
        dateToUse.setHours(date.getHours() - date.getTimezoneOffset() / 60);

        return dateToUse.getHours();
      };

      return `${getHours()}:${date.getMinutes()}`;
    }

    case PLANTS_MODULE_DURATION_WEEK:
    case PLANTS_MODULE_DURATION_MONTH: {
      return `${date.getDate()}.${date.getMonth() + 1}`;
    }

    default: {
      return "";
    }
  }
};

export const getWateringModeDescription = (
  mode: PlantMode | undefined,
  time: string | undefined,
  wateringMode: WateringMode | undefined
): string => {
  switch (mode) {
    case PLANT_MODE_TIME: {
      return `Used simple - time based watering mode. Plant is being watered every ${time} hours.`;
    }
    case PLANT_MODE_INTERACTIVE: {
      const values = WATERING_MODES_VALUES_LIST.find(
        (values) => values.id === wateringMode?.id
      );

      return `Used adaptive watering mode. Plant environment must not be lower than following values:\n\n• Min soil humidity: ${values?.minSoilHum} %\n• Min soil temperature: ${values?.minSoilTemp} °C\n• Min air humidity: ${values?.minAirHum} %\n• Min air temperature: ${values?.minAirTemp} °C`;
    }
    default: {
      return "";
    }
  }
};
