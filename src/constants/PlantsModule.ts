export const PLANT_MODE_TIME = "TIME";
export const PLANT_MODE_INTERACTIVE = "INTERACTIVE";

export const PLANT_MODES = [PLANT_MODE_TIME, PLANT_MODE_INTERACTIVE];

export const REAL_TIME_ROOM_TOPIC = "room_state";
export const REAL_TIME_PLANT_TOPIC = "plant_state";

export const PLANTS_MODULE_DURATION_DAY = "DAY";
export const PLANTS_MODULE_DURATION_WEEK = "WEEK";
export const PLANTS_MODULE_DURATION_MONTH = "MONTH";

export const LINE_CHART_DURATION_OPTIONS = [
  {
    label: "Day",
    value: PLANTS_MODULE_DURATION_DAY,
  },
  {
    label: "Week",
    value: PLANTS_MODULE_DURATION_WEEK,
  },
  {
    label: "Month",
    value: PLANTS_MODULE_DURATION_MONTH,
  },
];

export const WATERING_MODES_VALUES_LIST = [
  {
    id: 1,
    minSoilHum: 80,
    minSoilTemp: 22,
    minAirHum: 45,
    minAirTemp: 20,
  },
  {
    id: 2,
    minSoilHum: 60,
    minSoilTemp: 22,
    minAirHum: 40,
    minAirTemp: 20,
  },
  {
    id: 3,
    minSoilHum: 45,
    minSoilTemp: 22,
    minAirHum: 35,
    minAirTemp: 20,
  },
  {
    id: 4,
    minSoilHum: 30,
    minSoilTemp: 22,
    minAirHum: 20,
    minAirTemp: 20,
  },
];
