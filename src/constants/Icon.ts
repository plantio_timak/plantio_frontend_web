export const USER_ICON = "User";
export const LOGOUT_ICON = "Logout";
export const EDIT_ICON = "Edit";
export const BACK_ICON = "Back";
export const GRAPH_ICON = "Graph";
export const CHECK_ICON = "Check";

export const ICON_NORMAL = "normal";
export const ICON_BIG = "big";
export const ICON_SMALL = "small";
