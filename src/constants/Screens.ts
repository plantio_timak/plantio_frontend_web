// screen routes
export const DEFAULT_SCREEN_PATH = "/";
export const LOGIN_SCREEN_PATH = "/login";
export const REGISTER_SCREEN_PATH = "/register";
export const DASHBOARD_SCREEN_PATH = "/dashboard";
export const PLANT_DETAILS_SCREEN_PATH = "/plant/:id";
export const ADMIN_SCREEN_PATH = "/admin";
