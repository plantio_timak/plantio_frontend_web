export const PLANTS_MODULE = "PLANTS_MODULE";

export const SYSTEM_DESC =
  "PlantIO is IoT platform, which relieves you of worries with taking care of your household. Currently we support following modules:";

export const PLANTS_MODULE_DESC =
  "Plants module gives you control over the entire process of plants watering. The whole process is fully automated, your only concern will be to add water to the system.";

export const MODULES = [PLANTS_MODULE];
