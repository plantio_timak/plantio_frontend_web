export const LABEL_SMALLER = "smaller";
export const LABEL_SMALL = "small";
export const LABEL_NORMAL = "normal";
export const LABEL_BIG = "big";
export const LABEL_BIGGER = "bigger";
