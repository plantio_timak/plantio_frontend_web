import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { isEmpty } from "lodash";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Form from "../../templates/form/Form";
import Input from "../../atoms/input/Input";
import useNotifications from "../../../hooks/useNotifications";
import { FormFooterLink } from "../../../types/Form";
import { LoginParams } from "../../../types/LoginScreenTypes";
import { onChangeHandler } from "../../../utils/Form";
import {
  LOGIN_SCREEN_PATH,
  REGISTER_SCREEN_PATH,
} from "../../../constants/Screens";
import { userLoginActionCreator } from "../../../redux/modules/user/actions";
import { StoreState } from "../../../types/StoreState";
import { getUserIsFetchingSelector } from "../../../redux/modules/user/reducer";

const LoginScreenContent: React.FC = () => {
  const {
    register,
    handleSubmit,
    errors,
    setValue,
    watch,
  } = useForm<LoginParams>();
  const history = useHistory();
  const dispatch = useDispatch();
  const { notify } = useNotifications();

  const isFetching = useSelector((state: StoreState) =>
    getUserIsFetchingSelector(state.user)
  );

  useEffect(() => {
    register(
      { name: "userName" },
      {
        required: "Username is required field",
      }
    );
    register(
      { name: "password" },
      {
        required: "Password is required field",
      }
    );
  }, [register]);

  const footerLinks: Array<FormFooterLink> = [
    {
      to: LOGIN_SCREEN_PATH,
      label: "Forgot password?",
    },
    {
      to: REGISTER_SCREEN_PATH,
      label: "Not a member yet?",
    },
  ];

  const { userName, password } = watch();

  const onSubmit = (values: LoginParams) => {
    const redirectToDashboard = (path: string) => history.push(path);

    dispatch(userLoginActionCreator(values, notify, redirectToDashboard));
  };
  // TODO : add error displaying
  return (
    <Form
      label="Login"
      buttonLabel="Log in"
      footerLinks={footerLinks}
      onSubmit={handleSubmit(onSubmit)}
      submitDisabled={!isEmpty(errors) || isFetching}
    >
      <Input
        name="userName"
        label="Username"
        error={errors?.userName?.message}
        value={userName}
        onChange={onChangeHandler(setValue)}
      />
      <Input
        label="Password"
        type="password"
        name="password"
        value={password}
        onChange={onChangeHandler(setValue)}
        error={errors?.password?.message}
      />
    </Form>
  );
};

export default LoginScreenContent;
