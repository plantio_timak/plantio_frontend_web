import React from "react";
import { useSelector } from "react-redux";

import PlantsModuleDashboardScreenFetcher from "../../modules/PlantsModule/DashboardScreen/DashboardScreenFetcher";
import EmptyDashboardContent from "./EmptyDashboardContent";
import { PLANTS_MODULE } from "../../../constants/Services";
import { getUserSelectedServiceSelector } from "../../../redux/modules/user/reducer";
import { StoreState } from "../../../types/StoreState";

const DashboardScreenContent: React.FC = () => {
  const selectedService = useSelector((state: StoreState) =>
    getUserSelectedServiceSelector(state.user)
  );

  const renderContent = () => {
    switch (selectedService) {
      case PLANTS_MODULE:
        return <PlantsModuleDashboardScreenFetcher />;
      default:
        return <EmptyDashboardContent />;
    }
  };

  return renderContent();
};

export default DashboardScreenContent;
