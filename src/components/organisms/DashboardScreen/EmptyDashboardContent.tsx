import React, { useEffect, useState } from "react";
import isEmpty from "lodash/isEmpty";
import { useForm } from "react-hook-form";
import { AxiosError, AxiosResponse } from "axios";
import { useDispatch, useSelector } from "react-redux";

import Label from "../../atoms/label/Label";
import Form from "../../templates/form/Form";
import Input from "../../atoms/input/Input";
import Select from "../../atoms/select/Select";
import useCheckErrorCode from "../../../hooks/useCheckErrorCode";
import useNotifications from "../../../hooks/useNotifications";
import { LABEL_NORMAL } from "../../../constants/Label";
import {
  MODULES,
  PLANTS_MODULE,
  PLANTS_MODULE_DESC,
  SYSTEM_DESC,
} from "../../../constants/Services";
import { onChangeHandler, onSelectChangeHander } from "../../../utils/Form";
import { AddModuleParams } from "../../../types/DashboardScreenTypes";
import { getNavigationModuleLabel } from "../../../utils/Navigation";
import { api } from "../../../api/api";
import { getRoomAddToUserApiPath } from "../../../api/apiPaths";
import { StoreState } from "../../../types/StoreState";
import { getUserTokenSelector } from "../../../redux/modules/user/reducer";
import { ONLY_NUMBERS_REGEX } from "../../../constants/Regex";
import {
  DEVICE_ALREADY_ASSIGNED_ERROR_CODE,
  ID_NOT_FOUND_ERROR_CODE,
} from "../../../constants/ErrorCodes";
import { ERROR_NOTIF, SUCCESS_NOTIF } from "../../../constants/Notifications";
import {
  plantsModuleFetchRoomsListSuccessActionCreator,
  plantsModuleSetSelectedRoomActionCreator,
} from "../../../redux/modules/plantsModule/actions";
import {
  userAddServiceActionCreator,
  userSetSelectedServiceActionCreator,
} from "../../../redux/modules/user/actions";
import styles from "./EmptyDashboardContent.module.scss";

const EmptyDashboardContent: React.FC = () => {
  const dispatch = useDispatch();
  const { checkErrorCode } = useCheckErrorCode();
  const { notify } = useNotifications();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );

  const {
    register,
    handleSubmit,
    errors,
    setValue,
    watch,
  } = useForm<AddModuleParams>();

  const [isModuleAdding, setIsModuleAdding] = useState(false);

  useEffect(() => {
    register(
      { name: "deviceId" },
      {
        required: "Module ID is required field",
        pattern: {
          value: ONLY_NUMBERS_REGEX,
          message: "Module ID should contain only numbers",
        },
      }
    );
    register(
      { name: "serviceType" },
      {
        required: "Module type is required field",
      }
    );
  }, [register]);

  const getModuleTypeOptions = () =>
    MODULES.map((module) => ({
      value: module,
      label: getNavigationModuleLabel(module),
    }));

  const moduleTypeOptions = getModuleTypeOptions();

  const { deviceId, serviceType } = watch();

  const getErrorMessage = (errorCode: string) => {
    switch (errorCode) {
      case ID_NOT_FOUND_ERROR_CODE:
        return "Module with requested ID is not existing";
      case DEVICE_ALREADY_ASSIGNED_ERROR_CODE:
        return "Module with requested ID is already assinged to user";
      default:
        return "Error occured during adding module to user";
    }
  };

  const onSubmit = (values: AddModuleParams) => {
    setIsModuleAdding(false);

    api(token)
      .post(getRoomAddToUserApiPath(), values)
      .then((response: AxiosResponse) => {
        setIsModuleAdding(false);

        // Refactor (move to separate function) when we will have more modules which could be added
        if (values.serviceType === PLANTS_MODULE) {
          dispatch(
            plantsModuleFetchRoomsListSuccessActionCreator(
              response.data?.userRooms
            )
          );
          dispatch(
            plantsModuleSetSelectedRoomActionCreator(
              parseInt(values.deviceId),
              token,
              notify
            )
          );
          dispatch(userAddServiceActionCreator(PLANTS_MODULE));
          dispatch(userSetSelectedServiceActionCreator(PLANTS_MODULE));
        }

        notify(
          `Service ${getNavigationModuleLabel(values.serviceType)} - ${
            values.deviceId
          } successfully addded`,
          SUCCESS_NOTIF
        );
      })
      .catch((error: AxiosError) => {
        setIsModuleAdding(false);

        const sessionErrorCode = error?.response?.data?.error;
        const [errorObject = null] = error?.response?.data?.errors || [];

        checkErrorCode(sessionErrorCode);

        if (errorObject) {
          const message = getErrorMessage(errorObject?.error);

          notify(message, ERROR_NOTIF);
        }
      });
  };

  return (
    <div className={styles.container}>
      <div className={styles.desc}>
        <Label text="PlantIO" />
        <div className={styles.descContent}>
          <Label text={SYSTEM_DESC} size={LABEL_NORMAL} />
          <Label
            text="Plants module •"
            size={LABEL_NORMAL}
            className={styles.descModuleName}
          />
          <Label text={PLANTS_MODULE_DESC} size={LABEL_NORMAL} />
        </div>
      </div>
      <div>
        <Form
          label="Add PlantIO module"
          buttonLabel="Add module"
          onSubmit={handleSubmit(onSubmit)}
          submitDisabled={!isEmpty(errors) || isModuleAdding}
        >
          <Select
            label="Module type"
            options={moduleTypeOptions}
            value={serviceType}
            error={errors?.serviceType?.message}
            onChange={onSelectChangeHander(setValue, "serviceType")}
          />
          <Input
            name="deviceId"
            label="Module ID"
            value={deviceId}
            error={errors?.deviceId?.message}
            onChange={onChangeHandler(setValue)}
          />
        </Form>
      </div>
    </div>
  );
};

export default EmptyDashboardContent;
