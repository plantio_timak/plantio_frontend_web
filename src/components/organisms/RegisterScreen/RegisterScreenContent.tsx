import React, { useEffect } from "react";
import { useForm } from "react-hook-form";
import { isEmpty } from "lodash";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";

import Form from "../../templates/form/Form";
import Input from "../../atoms/input/Input";
import useNotifications from "../../../hooks/useNotifications";
import { FormFooterLink } from "../../../types/Form";
import { RegisterParams } from "../../../types/RegisterScreenTypes";
import { onChangeHandler } from "../../../utils/Form";
import {
  DASHBOARD_SCREEN_PATH,
  LOGIN_SCREEN_PATH,
} from "../../../constants/Screens";
import { EMAIL_REGEX } from "../../../constants/Regex";
import { StoreState } from "../../../types/StoreState";
import { getUserIsFetchingSelector } from "../../../redux/modules/user/reducer";
import { userRegisterActionCreator } from "../../../redux/modules/user/actions";

const RegisterScreenContent: React.FC = () => {
  const {
    register,
    handleSubmit,
    errors,
    setValue,
    getValues,
    watch,
  } = useForm<RegisterParams>();
  const history = useHistory();
  const dispatch = useDispatch();
  const { notify } = useNotifications();

  const isFetching = useSelector((state: StoreState) =>
    getUserIsFetchingSelector(state.user)
  );

  useEffect(() => {
    register(
      { name: "userName" },
      {
        required: "Username is required field",
        minLength: {
          value: 4,
          message: "Username should be at least 4 chars long",
        },
      }
    );
    register(
      { name: "email" },
      {
        required: "Email is required field",
        pattern: {
          value: EMAIL_REGEX,
          message: "Filled email is in wrong format",
        },
      }
    );
    register(
      { name: "password" },
      {
        required: "Password is required field",
        minLength: {
          value: 4,
          message: "Username should be at least 4 chars long",
        },
      }
    );
    register(
      { name: "confirmPassword" },
      {
        required: "Confirm password is required field",
        validate: (value) => {
          const values = getValues();
          const notMatchPassword = value !== values.password;

          if (notMatchPassword) {
            return "Password confirmation should match password";
          }

          return undefined;
        },
      }
    );
  }, [register, getValues]);

  const footerLinks: Array<FormFooterLink> = [
    {
      to: LOGIN_SCREEN_PATH,
      label: "Already member?",
    },
  ];

  const onSubmit = ({ userName, password, email }: RegisterParams) => {
    const filteredValues = {
      userName,
      password,
      email,
    };

    const redirectToDashboard = () => history.push(DASHBOARD_SCREEN_PATH);

    dispatch(
      userRegisterActionCreator(filteredValues, notify, redirectToDashboard)
    );
  };

  const { userName, email, password, confirmPassword } = watch();
  // TODO : add error displaying
  return (
    <Form
      label="Register"
      buttonLabel="Register"
      footerLinks={footerLinks}
      onSubmit={handleSubmit(onSubmit)}
      submitDisabled={!isEmpty(errors) || isFetching}
    >
      <Input
        name="userName"
        label="Username"
        value={userName}
        error={errors?.userName?.message}
        onChange={onChangeHandler(setValue)}
      />
      <Input
        name="email"
        label="Email"
        value={email}
        error={errors?.email?.message}
        onChange={onChangeHandler(setValue)}
      />
      <Input
        label="Password"
        type="password"
        name="password"
        value={password}
        onChange={onChangeHandler(setValue)}
        error={errors?.password?.message}
      />
      <Input
        label="Confirm password"
        type="password"
        name="confirmPassword"
        value={confirmPassword}
        onChange={onChangeHandler(setValue)}
        error={errors?.confirmPassword?.message}
      />
    </Form>
  );
};

export default RegisterScreenContent;
