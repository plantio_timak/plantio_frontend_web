import React from "react";

import ListPickerItem from "./ListPickerItem";
import { ListPickerItemType } from "../../../types/ListPicker";
import styles from "./ListPicker.module.scss";

type Props = {
  items: Array<ListPickerItemType>;
  selectedItem: ListPickerItemType | null;
  onItemClick: (value: ListPickerItemType) => void;
};

const ListPicker: React.FC<Props> = ({ items, selectedItem, onItemClick }) => (
  <div className={styles.container}>
    {items?.map((item) => (
      <ListPickerItem
        value={item}
        key={item.value}
        onClick={onItemClick}
        isSelected={item.value === selectedItem?.value}
      />
    ))}
  </div>
);

export default ListPicker;
