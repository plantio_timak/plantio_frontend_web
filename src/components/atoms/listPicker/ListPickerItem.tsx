import React from "react";

import { ListPickerItemType } from "../../../types/ListPicker";
import styles from "./ListPicker.module.scss";

type Props = {
  value: ListPickerItemType;
  onClick: (value: ListPickerItemType) => void;
  isSelected: boolean;
};

const ListPickerItem: React.FC<Props> = ({ value, onClick, isSelected }) => {
  const handleClick = () => {
    onClick(value);
  };

  const getClassName = () => {
    if (isSelected) {
      return `${styles.item} ${styles.itemActive}`;
    }

    return styles.item;
  };

  const className = getClassName();

  return (
    <button className={className} onClick={handleClick}>
      {value?.label}
    </button>
  );
};

export default ListPickerItem;
