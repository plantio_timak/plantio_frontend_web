import React, { useEffect, useState } from "react";
import {
  CartesianGrid,
  Line as LineBase,
  LineChart as LineChartBase,
  ResponsiveContainer,
  Tooltip,
  XAxis,
  YAxis,
} from "recharts";
import { useDispatch, useSelector } from "react-redux";

import IconButton from "../iconButton/IconButton";
import Label from "../label/Label";
import SettingsModal from "./SettingsModal/SettingsModal";
import useWindowDimensions from "../../../hooks/useWindowDimensions";
import { BUTTON_SECONDARY } from "../../../constants/Button";
import { GRAPH_ICON } from "../../../constants/Icon";
import { LABEL_SMALL } from "../../../constants/Label";
import { COLOR_SECONDARY } from "../../../styles/Colors";
import { FONT_SIZE_SMALL } from "../../../styles/Fonts";
import { Line, LineChartData } from "../../../types/LineChart";
import { LINE_COLORS } from "../../../constants/LineChart";
import { PlantsModuleDataDuration } from "../../../types/PlantsModule";
import { plantsModuleSetDataDurationActionCreator } from "../../../redux/modules/plantsModule/actions";
import { StoreState } from "../../../types/StoreState";
import { getPlantsModuleDataDurationSelector } from "../../../redux/modules/plantsModule/reducer";
import styles from "./LineChart.module.scss";
import { formatLineChartXTick } from "../../../utils/PlantsModule";

type Props<T extends Record<string, unknown>> = {
  data: LineChartData<T>;
  xLabel: string;
  yLabel: string;
  xKey: string;
  yUnit?: string;
};

const LineChart = <T extends Record<string, unknown>>({
  xLabel,
  yLabel,
  xKey,
  yUnit,
  data,
}: Props<T>): React.ReactElement => {
  const getLinesData = (): Array<Line> =>
    data.keys.map((item) => {
      return {
        ...item,
        visible: item.defaultVisible,
      };
    });

  const dispatch = useDispatch();
  const { height } = useWindowDimensions();

  const dataDuration = useSelector((state: StoreState) =>
    getPlantsModuleDataDurationSelector(state.plantsModule)
  );

  const [settingsModalVisible, setSettingsModalVisible] = useState(false);
  const [linesState, setLinesState] = useState(getLinesData());

  const formatTooltipLabel = (value: Date) => {
    if (value.getHours) {
      const dateToUse = new Date(value);
      dateToUse.setHours(value.getHours() - value.getTimezoneOffset() / 60);

      return `${xLabel}: ${dateToUse.toLocaleString("sk-SK")}`;
    }

    return "";
  };
  const formatTooltipValue = (value: number, key: string) => {
    const label = linesState.find((line) => line.value === key)?.label;

    return [`${value.toFixed(2)} ${yUnit}`, label];
  };

  const handleConfirmChangeSettings = (
    lines: Array<Line>,
    dataDuration: PlantsModuleDataDuration
  ) => {
    setLinesState(lines);
    dispatch(plantsModuleSetDataDurationActionCreator(dataDuration));
    setSettingsModalVisible(false);
  };

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  const formatTick = (currentTime: any) => {
    if (currentTime.getDate) {
      return formatLineChartXTick(currentTime, dataDuration);
    }

    return "";
  };

  useEffect(() => {
    setLinesState(getLinesData());
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [data]);

  const tickCount = Math.round(data.data.length / 10);

  return (
    <>
      <div className={styles.wrapper}>
        <div className={styles.container}>
          <div className={styles.header}>
            <Label text={`${yLabel} ${yUnit}`} size={LABEL_SMALL} />
            <IconButton
              iconName={GRAPH_ICON}
              type={BUTTON_SECONDARY}
              onClick={() => setSettingsModalVisible(true)}
            />
          </div>
          <ResponsiveContainer height={height / 4}>
            <LineChartBase
              data={data.data}
              margin={{
                top: 20,
                right: 10,
                left: 10,
              }}
            >
              <CartesianGrid vertical={false} stroke={COLOR_SECONDARY} />
              <XAxis
                fontSize={FONT_SIZE_SMALL}
                tickLine={false}
                stroke={COLOR_SECONDARY}
                strokeWidth={3}
                dataKey={xKey}
                tickFormatter={formatTick}
                interval={tickCount}
              />
              <YAxis
                name={yLabel}
                stroke={COLOR_SECONDARY}
                axisLine={false}
                fontSize={FONT_SIZE_SMALL}
                tick={{ dx: -20 }}
              />
              <Tooltip
                labelFormatter={formatTooltipLabel}
                formatter={formatTooltipValue}
              />
              {linesState.map(
                (line, index) =>
                  line.visible && (
                    <LineBase
                      key={line.value}
                      strokeWidth={3}
                      type="monotone"
                      dataKey={line.value}
                      stroke={LINE_COLORS[index % 5]}
                      dot={false}
                      activeDot={{ r: 8 }}
                      connectNulls
                    />
                  )
              )}
            </LineChartBase>
          </ResponsiveContainer>
        </div>
      </div>
      <SettingsModal
        visible={settingsModalVisible}
        lines={linesState}
        dataDuration={dataDuration}
        setIsModalVisible={setSettingsModalVisible}
        onConfirm={handleConfirmChangeSettings}
      />
    </>
  );
};

LineChart.defaultProps = {
  xUnit: "",
  yUnit: "",
};

export default LineChart;
