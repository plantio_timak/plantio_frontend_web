import React, { useState } from "react";

import Modal from "../../../templates/modal/Modal";
import Button from "../../button/Button";
import Select from "../../select/Select";
import Label from "../../label/Label";
import Checkbox from "../../checkbox/Checkbox";
import { BUTTON_SECONDARY, BUTTON_SMALL } from "../../../../constants/Button";
import { LABEL_NORMAL, LABEL_SMALL } from "../../../../constants/Label";
import styles from "./SettingsModal.module.scss";
import { Line } from "../../../../types/LineChart";
import { LINE_COLORS } from "../../../../constants/LineChart";
import { LINE_CHART_DURATION_OPTIONS } from "../../../../constants/PlantsModule";
import { PlantsModuleDataDuration } from "../../../../types/PlantsModule";

type Props = {
  visible: boolean;
  lines: Array<Line>;
  dataDuration: PlantsModuleDataDuration;
  setIsModalVisible: (visible: boolean) => void;
  onConfirm: (lines: Array<Line>, duration: PlantsModuleDataDuration) => void;
};

const SettingsModal: React.FC<Props> = ({
  visible,
  lines,
  dataDuration,
  setIsModalVisible,
  onConfirm,
}) => {
  const [linesState, setLinesState] = useState(lines);
  const [duration, setDuration] = useState(dataDuration);

  const updateLine = (lineToUpdate: Line) => (visible: boolean) => {
    const updatedLines = linesState.map((line) => {
      if (line.label === lineToUpdate.label) {
        return { ...line, visible };
      }

      return line;
    });
    setLinesState(updatedLines);
  };

  const renderLineCheckbox = (line: Line, index: number) => {
    const colorIndex = index % 5;

    return (
      <div className={styles.lineCheckboxContainer} key={line.label}>
        <Checkbox value={line.visible} onChange={updateLine(line)} />
        <div className={styles.lineContainer}>
          <span
            className={styles.dot}
            style={{ backgroundColor: LINE_COLORS[colorIndex] }}
          />
          <span
            className={styles.line}
            style={{ borderColor: LINE_COLORS[colorIndex] }}
          />
          <span
            className={styles.dot}
            style={{ backgroundColor: LINE_COLORS[colorIndex] }}
          />
        </div>
        <Label text={line.label} size={LABEL_SMALL} />
      </div>
    );
  };

  return (
    <Modal visible={visible} autoWidth>
      <Modal.Header label="Graph settings" />
      <Modal.Body>
        <Select
          label="Duration"
          options={LINE_CHART_DURATION_OPTIONS}
          value={duration}
          onChange={(value) => setDuration(value as PlantsModuleDataDuration)}
        />
        <Label text="Lines" size={LABEL_NORMAL} />
        {linesState.map((line, index) => renderLineCheckbox(line, index))}
      </Modal.Body>
      <Modal.Footer>
        <Button
          label="Confirm"
          onClick={() => onConfirm(linesState, duration)}
          size={BUTTON_SMALL}
        />
        <Button
          label="Cancel"
          onClick={() => setIsModalVisible(false)}
          size={BUTTON_SMALL}
          type={BUTTON_SECONDARY}
        />
      </Modal.Footer>
    </Modal>
  );
};

export default SettingsModal;
