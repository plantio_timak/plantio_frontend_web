import React from "react";
import { Link as LinkBase } from "react-router-dom";

import styles from "./Link.module.scss";

type Props = {
  to: string;
  label: string;
};

const Link: React.FC<Props> = ({ to, label }) => {
  return (
    <LinkBase className={styles.link} to={to}>
      {label}
    </LinkBase>
  );
};

export default Link;
