import React from "react";

import Label from "../label/Label";
import { ButtonSize, ButtonType } from "../../../types/Button";
import {
  BUTTON_NORMAL,
  BUTTON_PRIMARY,
  BUTTON_SECONDARY,
  BUTTON_SMALL,
} from "../../../constants/Button";
import styles from "./Button.module.scss";

type Props = {
  label: string;
  size?: ButtonSize;
  type?: ButtonType;
  disabled?: boolean;
  autoWidth?: boolean;
  onClick: () => void;
};

const Button: React.FC<Props> = ({
  label,
  onClick,
  disabled,
  size,
  type,
  autoWidth,
}) => {
  const getLabelClassName = () => {
    switch (size) {
      case BUTTON_SMALL:
        return styles.buttonSmallLabel;

      default: {
        return null;
      }
    }
  };

  const getTypeClassName = () => {
    switch (type) {
      case BUTTON_SECONDARY:
        return styles.buttonSecondary;

      default: {
        return styles.buttonPrimary;
      }
    }
  };

  const getSizeClassName = () => {
    switch (size) {
      case BUTTON_SMALL:
        return styles.buttonSmall;

      default: {
        return null;
      }
    }
  };

  const getWidthClassName = () => {
    if (autoWidth) {
      return styles.buttonAutoWidth;
    }

    return false;
  };

  const getClassNames = (): string => {
    const typeClassName = getTypeClassName();
    const sizeClassName = getSizeClassName();
    const widthClassName = getWidthClassName();

    const defaultClassNames = `${typeClassName} ${sizeClassName} ${widthClassName}`;

    if (disabled) {
      return `${defaultClassNames} ${styles.disabled}`;
    }

    return `${defaultClassNames}`;
  };

  const classNames = getClassNames();
  const labelClassName = getLabelClassName();

  return (
    <button
      className={classNames}
      onClick={onClick}
      disabled={disabled}
      type="button"
    >
      <Label text={label} className={labelClassName} />
    </button>
  );
};

Button.defaultProps = {
  size: BUTTON_NORMAL,
  type: BUTTON_PRIMARY,
  autoWidth: false,
};

export default Button;
