import React from "react";

import { ReactComponent as UserIcon } from "../../../assets/icons/User.svg";
import { ReactComponent as LogoutIcon } from "../../../assets/icons/Logout.svg";
import { ReactComponent as EditIcon } from "../../../assets/icons/Edit.svg";
import { ReactComponent as BackIcon } from "../../../assets/icons/Back.svg";
import { ReactComponent as GraphIcon } from "../../../assets/icons/Graph.svg";
import { ReactComponent as CheckIcon } from "../../../assets/icons/Check.svg";
import {
  BACK_ICON,
  CHECK_ICON,
  EDIT_ICON,
  GRAPH_ICON,
  ICON_BIG,
  ICON_NORMAL,
  ICON_SMALL,
  LOGOUT_ICON,
  USER_ICON,
} from "../../../constants/Icon";
import { IconName, IconSize } from "../../../types/Icon";
import styles from "./Icon.module.scss";

type Props = {
  name: IconName;
  size?: IconSize;
};

const Icon: React.FC<Props> = ({ name, size }) => {
  const getClassName = (): string => {
    switch (size) {
      case ICON_BIG:
        return styles.iconBig;

      case ICON_SMALL:
        return styles.iconSmall;

      default:
        return styles.iconNormal;
    }
  };

  const className = getClassName();

  const renderIcon = () => {
    switch (name) {
      case USER_ICON:
        return <UserIcon className={className} />;

      case LOGOUT_ICON:
        return <LogoutIcon className={className} />;

      case EDIT_ICON:
        return <EditIcon className={className} />;

      case BACK_ICON:
        return <BackIcon className={className} />;

      case GRAPH_ICON:
        return <GraphIcon className={className} />;

      case CHECK_ICON:
        return <CheckIcon className={className} />;

      default:
        return null;
    }
  };

  return renderIcon();
};

Icon.defaultProps = {
  size: ICON_NORMAL,
};

export default Icon;
