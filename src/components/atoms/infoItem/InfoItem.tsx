import React from "react";

import Label from "../label/Label";
import { INFO_ITEM_NORMAL, INFO_ITEM_SMALL } from "../../../constants/InfoItem";
import { LABEL_NORMAL, LABEL_SMALL } from "../../../constants/Label";
import { InfoItemSize } from "../../../types/InfoItem";
import styles from "./InfoItem.module.scss";

type Props = {
  label: string;
  value: string | number | undefined;
  unit?: string;
  size?: InfoItemSize;
  className?: string;
};

const InfoItem: React.FC<Props> = ({ value, label, size, className, unit }) => {
  const getFontSize = () => {
    if (size === INFO_ITEM_SMALL) {
      return LABEL_SMALL;
    }

    return LABEL_NORMAL;
  };

  const roundValue = (value: string | number) => {
    if (typeof value === "number") {
      return value.toFixed(2);
    }

    return value;
  };

  const formatValue = (): string | number => {
    if (!value && value !== 0) {
      return "";
    }

    const roundedValue = roundValue(value);

    if (unit) {
      return `${roundedValue} ${unit}`;
    }

    return roundedValue;
  };

  const fontSize = getFontSize();
  const formatedValue = formatValue();

  return (
    <div className={`${styles.container} ${className}`}>
      <Label text={label} size={fontSize} />
      <Label text={formatedValue} size={fontSize} className={styles.value} />
    </div>
  );
};

InfoItem.defaultProps = {
  size: INFO_ITEM_NORMAL,
};

export default InfoItem;
