import React from "react";

import Icon from "../icon/Icon";
import Label from "../label/Label";
import { IconName, IconSize } from "../../../types/Icon";
import { ButtonType } from "../../../types/Button";
import { ICON_NORMAL } from "../../../constants/Icon";
import { BUTTON_PRIMARY, BUTTON_SECONDARY } from "../../../constants/Button";
import styles from "./IconButton.module.scss";

type Props = {
  iconName: IconName;
  iconSize?: IconSize;
  label?: string | null;
  type?: ButtonType;
  onClick: () => void;
};

const IconButton: React.FC<Props> = ({
  iconName,
  label,
  onClick,
  iconSize,
  type,
}) => {
  const getClassNames = () => {
    switch (type) {
      case BUTTON_SECONDARY:
        return `${styles.button} ${styles.buttonSecondary}`;

      default:
        return `${styles.button} ${styles.buttonPrimary}`;
    }
  };

  const classNames = getClassNames();

  return (
    <button className={classNames} onClick={onClick}>
      <Icon name={iconName} size={iconSize} />
      {label && <Label text={label} />}
    </button>
  );
};

IconButton.defaultProps = {
  iconSize: ICON_NORMAL,
  type: BUTTON_PRIMARY,
};

export default IconButton;
