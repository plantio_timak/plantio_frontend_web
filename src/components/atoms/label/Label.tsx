import React from "react";

import { LabelSize } from "../../../types/Label";
import {
  LABEL_BIG,
  LABEL_BIGGER,
  LABEL_NORMAL,
  LABEL_SMALL,
  LABEL_SMALLER,
} from "../../../constants/Label";
import styles from "./Label.module.scss";

type Props = {
  text: string | number | null | undefined;
  size?: LabelSize;
  className?: string | null;
};

const Label: React.FC<Props> = ({ text, size, className }) => {
  const getClassNames = (): string => {
    const defaultClassName = `${styles.label} ${className || ""}`;

    switch (size) {
      case LABEL_SMALLER:
        return `${defaultClassName} ${styles.labelSmaller}`;
      case LABEL_SMALL:
        return `${defaultClassName} ${styles.labelSmall}`;
      case LABEL_NORMAL:
        return `${defaultClassName} ${styles.labelNormal}`;
      case LABEL_BIG:
        return `${defaultClassName} ${styles.labelBig}`;
      case LABEL_BIGGER:
        return `${defaultClassName} ${styles.labelBigger}`;
      default:
        return defaultClassName;
    }
  };

  const classNames = getClassNames();

  return <div className={classNames}>{text}</div>;
};

Label.defaultProps = {
  size: LABEL_BIG,
};

export default Label;
