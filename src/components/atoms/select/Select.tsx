import React from "react";
import SelectBase, { OptionTypeBase, StylesConfig } from "react-select";

import Label from "../label/Label";
import { LABEL_SMALLER } from "../../../constants/Label";
import {
  COLOR_BASE,
  COLOR_ERROR,
  COLOR_PRIMARY_LIGHTER,
  COLOR_SECONDARY,
  COLOR_SECONDARY_OPACITY,
} from "../../../styles/Colors";
import { FONT_SIZE_NORMAL } from "../../../styles/Fonts";
import styles from "./Select.module.scss";

type Props = {
  label: string;
  options: Array<OptionTypeBase>;
  onChange: (value: string | number) => void;
  value: number | string | null | undefined;
  error?: string;
};

const Select: React.FC<Props> = ({
  label,
  options,
  onChange,
  value,
  error,
}) => {
  const getContainerClassNames = (): string => {
    const defaultClassName = styles.container;

    if (error) {
      return `${defaultClassName} ${styles.error}`;
    }

    return defaultClassName;
  };

  const containerClassNames = getContainerClassNames();

  const handleChange = (option: OptionTypeBase | null) =>
    onChange(option?.value);

  const selectedValue = options.find((option) => option.value === value);
  const hasError = !!error;

  return (
    <>
      <div className={containerClassNames}>
        <Label text={label} size={LABEL_SMALLER} />
        <SelectBase
          value={selectedValue}
          styles={getStyles(hasError)}
          options={options}
          onChange={handleChange}
        />
      </div>
      {error && (
        <Label text={error} size={LABEL_SMALLER} className={styles.errorText} />
      )}
    </>
  );
};

const getStyles = (
  hasError: boolean
): StylesConfig<OptionTypeBase, boolean> => {
  const color = hasError ? COLOR_ERROR : COLOR_SECONDARY;

  return {
    control: () => ({
      display: "flex",
      borderBottom: `3px solid ${color}`,
      backgroundColor: COLOR_BASE,
      fontSize: FONT_SIZE_NORMAL,
      cursor: "pointer",
      paddingBottom: "0.5rem",
      paddingTop: "0.5rem",
    }),

    valueContainer: (provided) => ({
      ...provided,
      padding: 0,
    }),

    singleValue: (provided) => ({
      ...provided,
      color: color,
    }),

    indicatorSeparator: () => ({
      display: "none",
    }),

    indicatorsContainer: () => ({
      color: color,
      display: "flex",

      "&:hover": {
        color: COLOR_SECONDARY_OPACITY,
      },
    }),

    dropdownIndicator: () => ({
      display: "flex",
      justifyContent: "center",
      alignItems: "center",
    }),

    menu: (provided) => ({
      ...provided,
      margin: 0,
      borderRadius: 0,
      border: `1px solid ${COLOR_PRIMARY_LIGHTER}`,
      fontSize: FONT_SIZE_NORMAL,
      boxShadow: "0px 4px 4px rgba(0, 0, 0, 0.25);",
    }),

    menuList: () => ({
      padding: 0,
    }),

    option: (_, state) => {
      const backgroundColor = state?.isSelected
        ? COLOR_PRIMARY_LIGHTER
        : COLOR_BASE;
      const color = state?.isSelected
        ? COLOR_SECONDARY
        : COLOR_SECONDARY_OPACITY;

      return {
        padding: "1rem 0.5rem",
        color: color,
        cursor: "pointer",
        backgroundColor,

        "&:hover": {
          color: COLOR_SECONDARY,
          backgroundColor: COLOR_PRIMARY_LIGHTER,
        },
      };
    },
  };
};

export default Select;
