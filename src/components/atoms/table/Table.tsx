import React from "react";
import { Column, usePagination, useTable } from "react-table";

import styles from "./Table.module.scss";

type Props<T extends Record<string, unknown>> = {
  columns: Array<Column<T>>;
  data: Array<T>;
};

const Table = <T extends Record<string, unknown>>({
  columns,
  data,
}: Props<T>): React.ReactElement => {
  const { rows, prepareRow, headers } = useTable(
    {
      columns,
      data,
    },
    usePagination
  );

  const getTrClassName = (index: number) => {
    const isEven = index % 2 !== 0;

    if (isEven) {
      return `${styles.tr} ${styles.trOdd}`;
    }

    return styles.tr;
  };

  return (
    <table className={styles.table}>
      <thead className={styles.thead}>
        <tr className={styles.tr}>
          {headers.map((column) => (
            <th className={styles.th} key={column.id}>
              {column.render("Header")}
            </th>
          ))}
        </tr>
      </thead>
      <tbody>
        {rows.map((row, index) => {
          prepareRow(row);
          const className = getTrClassName(index);

          return (
            <tr className={className} key={row.id}>
              {row.cells.map((cell) => (
                <td className={styles.td} key={cell.getCellProps().key}>
                  {cell.render("Cell")}
                </td>
              ))}
            </tr>
          );
        })}
      </tbody>
    </table>
  );
};

export default Table;
