import React from "react";

import Label from "../label/Label";
import { LABEL_SMALLER } from "../../../constants/Label";
import styles from "./Input.module.scss";

type Props = {
  name: string;
  label: string;
  placeholder?: string;
  type?: string;
  error?: string;
  className?: string;
  value: string | null | undefined;
  onChange: (name: string, value: string) => void;
};

const Input: React.FC<Props> = ({
  label,
  placeholder,
  type,
  error,
  onChange,
  name,
  className,
  value,
}) => {
  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) =>
    onChange(event.target.name, event.target.value);

  const getContainerClassNames = (): string => {
    const defaultClassName = `${styles.inputContainer} ${className}`;

    if (error) {
      return `${defaultClassName} ${styles.error}`;
    }

    return defaultClassName;
  };

  const containerClassNames = getContainerClassNames();

  return (
    <>
      <div className={containerClassNames}>
        <Label text={label} size={LABEL_SMALLER} />
        <input
          name={name}
          className={styles.input}
          placeholder={placeholder}
          type={type}
          onChange={handleChange}
          value={value || ""}
        />
      </div>
      {error && (
        <Label text={error} size={LABEL_SMALLER} className={styles.errorText} />
      )}
    </>
  );
};

Input.defaultProps = {
  type: "text",
};

export default Input;
