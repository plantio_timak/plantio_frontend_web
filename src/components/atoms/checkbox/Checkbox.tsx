import React from "react";

import { CHECK_ICON, ICON_SMALL } from "../../../constants/Icon";
import Icon from "../icon/Icon";
import styles from "./Checkbox.module.scss";

type Props = {
  value: boolean;
  onChange: (value: boolean) => void;
};

const Checkbox: React.FC<Props> = ({ value, onChange }) => (
  <button className={styles.container} onClick={() => onChange(!value)}>
    {value && <Icon name={CHECK_ICON} size={ICON_SMALL} />}
  </button>
);

export default Checkbox;
