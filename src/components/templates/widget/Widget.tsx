import React from "react";

import IconButton from "../../atoms/iconButton/IconButton";
import Label from "../../atoms/label/Label";
import { BUTTON_SECONDARY } from "../../../constants/Button";
import { IconName } from "../../../types/Icon";
import styles from "./Widget.module.scss";

type Props = {
  title: string;
  className?: string;
  icon?: IconName;
  onIconClick?: () => void;
};

const Widget: React.FC<Props> = ({
  className,
  title,
  children,
  icon,
  onIconClick,
}) => {
  const renderIcon = () => {
    if (icon && onIconClick) {
      return (
        <IconButton
          iconName={icon}
          type={BUTTON_SECONDARY}
          onClick={onIconClick}
        />
      );
    }

    return null;
  };

  return (
    <div className={`${styles.container} ${className}`}>
      <div className={styles.content}>
        <div className={styles.title}>
          <Label text={title} />
          {renderIcon()}
        </div>
        {children}
      </div>
    </div>
  );
};

export default Widget;
