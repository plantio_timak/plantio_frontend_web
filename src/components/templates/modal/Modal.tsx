import React from "react";

import Label from "../../atoms/label/Label";
import styles from "./Modal.module.scss";

type Props = {
  visible: boolean;
  autoWidth?: boolean;
};

type HeaderProps = {
  label: string;
};

const Header: React.FC<HeaderProps> = ({ label }) => {
  return (
    <div className={styles.header}>
      <Label text={label} />
    </div>
  );
};

const Footer: React.FC = ({ children }) => {
  return <div className={styles.footer}>{children}</div>;
};

const Body: React.FC = ({ children }) => {
  return <div className={styles.body}>{children}</div>;
};

const Modal: React.FC<Props> & {
  Header: typeof Header;
  Footer: typeof Footer;
  Body: typeof Body;
} = ({ visible, children, autoWidth }) => {
  const getOverlayClassName = () => {
    if (visible) {
      return styles.overlay;
    }

    return `${styles.overlay} ${styles.closed}`;
  };

  const getContainerClassName = () => {
    if (autoWidth) {
      return `${styles.container} ${styles.autoWidth}`;
    }

    return styles.container;
  };

  const overlayClassName = getOverlayClassName();
  const containerClassName = getContainerClassName();

  return (
    <div className={overlayClassName}>
      <div className={containerClassName}>{children}</div>
    </div>
  );
};

Modal.Header = Header;
Modal.Footer = Footer;
Modal.Body = Body;

export default Modal;
