import React from "react";

import Label from "../../atoms/label/Label";
import Button from "../../atoms/button/Button";
import Link from "../../atoms/link/Link";
import { FormFooterLink } from "../../../types/Form";
import styles from "./Form.module.scss";

type Props = {
  label: string;
  buttonLabel: string;
  footerLinks?: Array<FormFooterLink>;
  onSubmit: () => void;
  submitDisabled?: boolean;
};

const Form: React.FC<Props> = ({
  label,
  buttonLabel,
  footerLinks,
  children,
  onSubmit,
  submitDisabled,
}) => {
  return (
    <form className={styles.form}>
      {label && <Label text={label} />}
      <div className={styles.fields}>{children}</div>
      <div>
        <Button
          label={buttonLabel}
          onClick={onSubmit}
          disabled={submitDisabled}
          autoWidth
        />
        {footerLinks && (
          <div className={styles.footerLinks}>
            {footerLinks?.map((footerLink) => (
              <Link
                key={footerLink.to}
                to={footerLink.to}
                label={footerLink.label}
              />
            ))}
          </div>
        )}
      </div>
    </form>
  );
};

export default Form;
