import React from "react";

import Header from "../../molecules/header/Header";
import Navigation from "../../molecules/navigation/Navigation";
import styles from "./Screen.module.scss";

type Props = {
  showNavigation?: boolean;
  showUser?: boolean;
};

const Screen: React.FC<Props> = ({ children, showNavigation, showUser }) => {
  const getClassName = () => {
    if (showNavigation) {
      return `${styles.screen} ${styles.screenNav}`;
    }

    return styles.screen;
  };

  const className = getClassName();

  return (
    <>
      <Header showUser={showUser} />
      {showNavigation && <Navigation />}
      <div className={className}>{children}</div>
    </>
  );
};

Screen.defaultProps = {
  showNavigation: true,
  showUser: true,
};

export default Screen;
