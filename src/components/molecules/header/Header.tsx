import React from "react";
import { useDispatch, useSelector } from "react-redux";

import IconButton from "../../atoms/iconButton/IconButton";
import Label from "../../atoms/label/Label";
import useNotifications from "../../../hooks/useNotifications";
import useEventSourceHook from "../../../hooks/useEventSourceHook";
import { userLogoutActionCreator } from "../../../redux/modules/user/actions";
import { getUserSelector } from "../../../redux/modules/user/reducer";
import { StoreState } from "../../../types/StoreState";
import { ICON_BIG, LOGOUT_ICON, USER_ICON } from "../../../constants/Icon";
import { LABEL_BIGGER } from "../../../constants/Label";
import { SUCCESS_NOTIF } from "../../../constants/Notifications";
import styles from "./Header.module.scss";

type Props = {
  showUser: boolean | undefined;
};

const Header: React.FC<Props> = ({ showUser }) => {
  const dispatch = useDispatch();
  const { notify } = useNotifications();
  const { closeEventSourceEmiter } = useEventSourceHook();

  const { userName } = useSelector((state: StoreState) =>
    getUserSelector(state.user)
  );

  const handleUserClick = () => {
    // TODO : add redirect to user details page or modal when will be ready
    return;
  };

  const handleLogout = () => {
    dispatch(userLogoutActionCreator());
    closeEventSourceEmiter();
    notify("User successfully logged out", SUCCESS_NOTIF);
  };

  return (
    <div className={styles.header}>
      <Label
        text="PlantIO"
        size={LABEL_BIGGER}
        className={styles.headerLabel}
      />
      {showUser && (
        <div className={styles.userHeader}>
          <IconButton
            iconName={USER_ICON}
            label={userName}
            onClick={handleUserClick}
          />
          <IconButton
            iconName={LOGOUT_ICON}
            iconSize={ICON_BIG}
            onClick={handleLogout}
          />
        </div>
      )}
    </div>
  );
};

export default Header;
