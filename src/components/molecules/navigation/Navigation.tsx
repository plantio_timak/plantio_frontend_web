import React from "react";
import { useDispatch, useSelector } from "react-redux";

import NavigationItem from "./NavigationItem";
import { StoreState } from "../../../types/StoreState";
import { Service } from "../../../types/Services";
import {
  getUserSelectedServiceSelector,
  getUserServicesSelector,
} from "../../../redux/modules/user/reducer";
import {
  getNavigationItemValue,
  getNavigationItems,
  getNavigationModuleLabel,
} from "../../../utils/Navigation";
import { userSetSelectedServiceActionCreator } from "../../../redux/modules/user/actions";
import styles from "./Navigation.module.scss";

const Navigation: React.FC = () => {
  const dispatch = useDispatch();
  const services = useSelector((state: StoreState) =>
    getUserServicesSelector(state.user)
  );
  const selectedService = useSelector((state: StoreState) =>
    getUserSelectedServiceSelector(state.user)
  );

  const navItems = getNavigationItems(services);

  const onItemClick = (item: string) => {
    const value = getNavigationItemValue(item);
    // TODO : send this value also to BE
    dispatch(userSetSelectedServiceActionCreator(value as Service));
  };

  const selectedServiceLabel = getNavigationModuleLabel(
    selectedService as string
  );

  return (
    <ul className={styles.nav}>
      {navItems?.map((item) => (
        <NavigationItem
          item={item}
          active={item === selectedServiceLabel}
          key={item}
          onClick={onItemClick}
        />
      ))}
    </ul>
  );
};

export default Navigation;
