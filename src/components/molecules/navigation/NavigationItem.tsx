import React from "react";

import Label from "../../atoms/label/Label";
import styles from "./Navigation.module.scss";

type Props = {
  item: string;
  active: boolean;
  onClick: (item: string) => void;
};

const NavigationItem: React.FC<Props> = ({ item, active, onClick }) => {
  const getClassName = () => {
    if (active) {
      return `${styles.navItem} ${styles.navItemActive}`;
    }

    return styles.navItem;
  };

  const handleClick = (): void => onClick(item);

  const className = getClassName();

  return (
    <li className={className} onClick={handleClick}>
      <Label text={item} />
    </li>
  );
};

export default NavigationItem;
