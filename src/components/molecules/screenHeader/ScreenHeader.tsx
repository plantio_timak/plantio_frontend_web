import React from "react";
import { useHistory } from "react-router-dom";

import Label from "../../atoms/label/Label";
import IconButton from "../../atoms/iconButton/IconButton";
import { LABEL_BIGGER } from "../../../constants/Label";
import { BACK_ICON, ICON_BIG } from "../../../constants/Icon";
import { BUTTON_SECONDARY } from "../../../constants/Button";
import styles from "./ScreenHeader.module.scss";

type Props = {
  title: string;
  hasBackButton?: boolean;
};

const ScreenHeader: React.FC<Props> = ({ title, children, hasBackButton }) => {
  const history = useHistory();

  return (
    <div className={styles.container}>
      <div className={styles.label}>
        {hasBackButton && (
          <IconButton
            iconName={BACK_ICON}
            iconSize={ICON_BIG}
            type={BUTTON_SECONDARY}
            onClick={() => history.goBack()}
          />
        )}
        <Label text={title} size={LABEL_BIGGER} />
      </div>
      {children}
    </div>
  );
};

ScreenHeader.defaultProps = {
  hasBackButton: false,
};

export default ScreenHeader;
