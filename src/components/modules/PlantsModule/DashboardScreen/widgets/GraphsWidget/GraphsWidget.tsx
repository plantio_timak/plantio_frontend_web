import React from "react";
import { useSelector } from "react-redux";

import LineChart from "../../../../../atoms/lineChart/LineChart";
import Widget from "../../../../../templates/widget/Widget";
import {
  getPlantsModulePlantsDataSelector,
  getPlantsModuleSelectedRoomDataSelector,
} from "../../../../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../../../../types/StoreState";
import styles from "./GraphsWidget.module.scss";
import { RoomDataApi } from "../../../../../../types/PlantsModule";

const GraphsWidget: React.FC = () => {
  const roomData = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomDataSelector(state.plantsModule)
  );
  const plantsData = useSelector((state: StoreState) =>
    getPlantsModulePlantsDataSelector(state.plantsModule)
  );

  const getMeasurements = (roomData: Array<RoomDataApi>, key: string) => {
    return (
      roomData?.map((measurement) => {
        const items: Record<string, number> = {};
        const date = new Date(measurement?.time);
        const values = measurement?.values as Record<string, number | boolean>;

        plantsData?.forEach((item) => {
          const valuesToUse = item.plantValues.find((value) => {
            const currentTime = value.time.split(".");
            const timeToCompare = measurement?.time.split(".");

            return (
              currentTime[0].slice(0, -2) === timeToCompare[0].slice(0, -2)
            );
          });

          const values = valuesToUse?.values as Record<string, number>;

          items[`${item.id}_${key}`] = values && values[key];
        });

        return {
          time: date,
          [key]: values[key],
          ...items,
        };
      }) || []
    );
  };

  const getDataKeys = (value: string, label: string) => {
    return plantsData.map((item) => {
      return {
        label: `${item.name} ${label}`,
        value: `${item.id}_${value}`,
        defaultVisible: false,
      };
    });
  };

  const tempData = {
    keys: [
      {
        label: "Temperature",
        value: "temperature",
        defaultVisible: true,
      },
      ...getDataKeys("temperature", "Temperature"),
    ],
    data: getMeasurements(roomData?.data || [], "temperature").reverse(),
  };

  const humData = {
    keys: [
      {
        label: "Humidity",
        value: "humidity",
        defaultVisible: true,
      },
      ...getDataKeys("humidity", "Humidity"),
    ],
    data: getMeasurements(roomData?.data || [], "humidity").reverse(),
  };

  return (
    <Widget title="Graphs" className={styles.container}>
      <div className={styles.content}>
        <LineChart
          data={tempData}
          xLabel="Time"
          yLabel="Temp"
          xKey="time"
          yUnit="°C"
        />
        <LineChart
          data={humData}
          xLabel="Time"
          yLabel="Humidity"
          xKey="time"
          yUnit="%"
        />
      </div>
    </Widget>
  );
};

export default GraphsWidget;
