import React, { useMemo } from "react";
import { Cell, Column } from "react-table";
import { useSelector } from "react-redux";

import Table from "../../../../../atoms/table/Table";
import Widget from "../../../../../templates/widget/Widget";
import Link from "../../../../../atoms/link/Link";
import {
  getLastPlantValueProperty,
  getWateringModeLabel,
} from "../../../../../../utils/PlantsModule";
import { StoreState } from "../../../../../../types/StoreState";
import { getPlantsModuleSelectedRoomPlantsDataSelector } from "../../../../../../redux/modules/plantsModule/reducer";
import { PlantData } from "../../../../../../types/PlantsModule";
import { getPlantDetailsRoutePath } from "../../../../../../utils/Screens";
import styles from "./PlantsWidget.module.scss";

const PlantsWidget: React.FC = () => {
  const plants = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomPlantsDataSelector(state.plantsModule)
  );

  const renderLink = ({ value, row }: Cell) => {
    const { id } = row.original as PlantData;

    return <Link label={value} to={getPlantDetailsRoutePath(id)} />;
  };

  const columns: Array<Column<PlantData>> = useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
        Cell: renderLink,
      },
      {
        Header: "Plant",
        accessor: "plant",
      },
      {
        Header: "Water mode",
        accessor: "mode",
        Cell: ({ value }: Cell) => getWateringModeLabel(value?.plantMode),
      },
      {
        id: "humidity",
        Header: "Soil humidity",
        accessor: "plantValues",
        Cell: ({ value }: Cell) => {
          const propertyValue = getLastPlantValueProperty(value, "humidity");

          if (!propertyValue) {
            return "";
          }

          return `${propertyValue} %`;
        },
      },
      {
        id: "temperature",
        Header: "Soil temperature",
        accessor: "plantValues",
        Cell: ({ value }: Cell) => {
          const propertyValue = getLastPlantValueProperty(value, "temperature");

          if (!propertyValue) {
            return "";
          }

          return `${propertyValue} °C`;
        },
      },
    ],
    []
  );

  return (
    <Widget title="Plants" className={styles.container}>
      <div className={styles.content}>
        <Table columns={columns} data={plants} />
      </div>
    </Widget>
  );
};

export default PlantsWidget;
