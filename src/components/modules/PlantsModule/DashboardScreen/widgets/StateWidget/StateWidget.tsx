import React from "react";
import isNil from "lodash/isNil";
import { useSelector } from "react-redux";

import Widget from "../../../../../templates/widget/Widget";
import InfoItem from "../../../../../atoms/infoItem/InfoItem";
import { getPlantsModuleSelectedRoomDataSelector } from "../../../../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../../../../types/StoreState";
import styles from "./StateWidget.module.scss";

const StateWidget: React.FC = () => {
  const roomData = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomDataSelector(state.plantsModule)
  );
  const [lastMeasuredValue] = roomData?.data || [];

  const getWaterCapacityValue = () => {
    if (!isNil(lastMeasuredValue?.values?.waterCapacity)) {
      if (lastMeasuredValue?.values?.waterCapacity) {
        return "Full";
      }

      return "Empty";
    }

    return "";
  };

  return (
    <Widget className={styles.container} title="State">
      <InfoItem
        label="Temperature"
        value={lastMeasuredValue?.values?.temperature}
        unit="°C"
      />
      <InfoItem
        label="Humidity"
        value={lastMeasuredValue?.values?.humidity}
        unit="%"
      />
      <InfoItem
        label="Light intensity"
        value={lastMeasuredValue?.values?.lightIntensity}
        unit="lux"
      />
      <InfoItem label="Water state" value={getWaterCapacityValue()} />
    </Widget>
  );
};

export default StateWidget;
