import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";

import Modal from "../../../../templates/modal/Modal";
import Button from "../../../../atoms/button/Button";
import AddRoomContent from "./AddRoomContent";
import SelectRoomFetcher from "./SelectRoomFetcher";
import useNotifications from "../../../../../hooks/useNotifications";
import {
  BUTTON_SECONDARY,
  BUTTON_SMALL,
} from "../../../../../constants/Button";
import { SUCCESS_NOTIF } from "../../../../../constants/Notifications";
import {
  plantsModuleFetchSelectedRoomDataSuccessActionCreator,
  plantsModuleSetSelectedRoomActionCreator,
} from "../../../../../redux/modules/plantsModule/actions";
import { RoomData } from "../../../../../types/PlantsModule";
import { ListPickerItemType } from "../../../../../types/ListPicker";
import { StoreState } from "../../../../../types/StoreState";
import { getPlantsModuleSelectedRoomSelector } from "../../../../../redux/modules/plantsModule/reducer";
import { getUserTokenSelector } from "../../../../../redux/modules/user/reducer";

type Props = {
  visible: boolean;
  setIsModalVisible: (visible: boolean) => void;
};

const AddOrSelectRoomModal: React.FC<Props> = ({
  visible,
  setIsModalVisible,
}) => {
  const dispatch = useDispatch();
  const { notify } = useNotifications();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const lastSelectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );
  const [selectedRoom, setSelectedRoom] = useState<ListPickerItemType | null>(
    null
  );
  const [selectedRoomData, setSelectedRoomData] = useState<RoomData | null>(
    null
  );

  useEffect(() => {
    if (lastSelectedRoom) {
      setSelectedRoom({
        value: lastSelectedRoom?.id,
        label: lastSelectedRoom?.name,
      });
    }
  }, [lastSelectedRoom]);

  const handleConfirmSelectedRoom = () => {
    dispatch(
      plantsModuleSetSelectedRoomActionCreator(
        selectedRoom?.value as number,
        token,
        notify
      )
    );
    dispatch(
      plantsModuleFetchSelectedRoomDataSuccessActionCreator(selectedRoomData)
    );
    notify(
      `Room was changed. Selected room: ${selectedRoom?.label}`,
      SUCCESS_NOTIF
    );
    setIsModalVisible(false);
  };

  return (
    <Modal visible={visible}>
      <Modal.Header label="Add or select room" />
      <Modal.Body>
        <AddRoomContent />
        <SelectRoomFetcher
          selectedRoom={selectedRoom}
          setSelectedRoom={setSelectedRoom}
          selectedRoomData={selectedRoomData as RoomData}
          setSelectedRoomData={setSelectedRoomData}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button
          label="Confirm"
          onClick={handleConfirmSelectedRoom}
          size={BUTTON_SMALL}
          disabled={!selectedRoom}
        />
        <Button
          label="Cancel"
          onClick={() => setIsModalVisible(false)}
          size={BUTTON_SMALL}
          type={BUTTON_SECONDARY}
        />
      </Modal.Footer>
    </Modal>
  );
};

export default AddOrSelectRoomModal;
