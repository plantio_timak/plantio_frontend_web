import { AxiosError, AxiosResponse } from "axios";
import React, { useEffect, useState } from "react";
import { useSelector } from "react-redux";

import SelectRoomContent from "./SelectRoomContent";
import useCheckErrorCode from "../../../../../hooks/useCheckErrorCode";
import useNotifications from "../../../../../hooks/useNotifications";
import { api } from "../../../../../api/api";
import {
  getRoomDataApiPath,
  getRoomPlantsDataApiPath,
} from "../../../../../api/apiPaths";
import { getUserTokenSelector } from "../../../../../redux/modules/user/reducer";
import { RoomData } from "../../../../../types/PlantsModule";
import { StoreState } from "../../../../../types/StoreState";
import { ROOM_NOT_EXIST_FOR_USER_ERROR_CODE } from "../../../../../constants/ErrorCodes";
import { ERROR_NOTIF } from "../../../../../constants/Notifications";
import {
  getPlantsModuleDataDurationSelector,
  getPlantsModuleRoomsListSelector,
} from "../../../../../redux/modules/plantsModule/reducer";
import { ListPickerItemType } from "../../../../../types/ListPicker";

type Props = {
  selectedRoom: ListPickerItemType | null;
  setSelectedRoom: (room: ListPickerItemType | null) => void;
  selectedRoomData: RoomData;
  setSelectedRoomData: (room: RoomData | null) => void;
};

const SelectRoomFetcher: React.FC<Props> = ({
  selectedRoom,
  setSelectedRoom,
  selectedRoomData,
  setSelectedRoomData,
}) => {
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const rooms = useSelector((state: StoreState) =>
    getPlantsModuleRoomsListSelector(state.plantsModule)
  );
  const dataDuration = useSelector((state: StoreState) =>
    getPlantsModuleDataDurationSelector(state.plantsModule)
  );
  const { checkErrorCode } = useCheckErrorCode();
  const { notify } = useNotifications();

  const [isRoomDataFetching, setRoomDataFetching] = useState(false);

  useEffect(() => {
    if (selectedRoom) {
      setRoomDataFetching(true);

      const fetchRoomData = api(token).get(
        getRoomDataApiPath(selectedRoom?.value as number, dataDuration)
      );
      const fetchRoomPlantsData = api(token).get(
        getRoomPlantsDataApiPath(selectedRoom?.value as number, dataDuration)
      );

      Promise.all([fetchRoomData, fetchRoomPlantsData])
        .then(
          ([
            { data: roomData },
            { data: roomPLantsData },
          ]: Array<AxiosResponse>) => {
            setRoomDataFetching(false);
            setSelectedRoomData({ data: roomData, ...roomPLantsData });
          }
        )
        .catch((error: AxiosError) => {
          setRoomDataFetching(false);
          const sessionErrorCode = error?.response?.data?.error;
          const [errorObject = null] = error?.response?.data?.errors || [];

          checkErrorCode(sessionErrorCode);

          if (errorObject) {
            const message =
              errorObject?.error === ROOM_NOT_EXIST_FOR_USER_ERROR_CODE
                ? "User is not assigned to selected room"
                : "Error occurred during fetching room data";

            notify(message, ERROR_NOTIF);
          }

          setSelectedRoomData(null);
          setSelectedRoom(null);
        });
    }
    // workaround for exhaustive depts - if dependency is added - rerenders for lot of times
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedRoom, token, setSelectedRoom]);

  return (
    <SelectRoomContent
      rooms={rooms}
      selectedRoom={selectedRoom}
      setSelectedRoom={setSelectedRoom}
      isSelectedRoomFetching={isRoomDataFetching}
      selectedRoomData={selectedRoomData}
    />
  );
};

export default SelectRoomFetcher;
