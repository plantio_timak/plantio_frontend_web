import React from "react";

import SelectRoomSelectedRoomContent from "./SelectRoomSelectedRoomContent";
import ListPicker from "../../../../atoms/listPicker/ListPicker";
import { Room, RoomData } from "../../../../../types/PlantsModule";
import { ListPickerItemType } from "../../../../../types/ListPicker";
import styles from "./AddOrSelectRoomModal.module.scss";

type Props = {
  rooms: Array<Room>;
  isSelectedRoomFetching: boolean;
  selectedRoom: ListPickerItemType | null;
  selectedRoomData: RoomData | null;
  setSelectedRoom: (room: ListPickerItemType) => void;
};

const SelectRoomContent: React.FC<Props> = ({
  rooms,
  selectedRoom,
  setSelectedRoom,
  isSelectedRoomFetching,
  selectedRoomData,
}) => {
  const getListPickerItems = (): Array<ListPickerItemType> =>
    rooms?.map((room) => ({
      value: room.id,
      label: room.name,
    }));

  const pickerItems = getListPickerItems();

  return (
    <div className={styles.selectRoomContainer}>
      <div className={styles.selectRoomPicker}>
        <ListPicker
          items={pickerItems}
          selectedItem={selectedRoom}
          onItemClick={(value) => setSelectedRoom(value)}
        />
      </div>
      <SelectRoomSelectedRoomContent
        selectedRoom={selectedRoom}
        selectedRoomData={selectedRoomData}
        isSelectedRoomFetching={isSelectedRoomFetching}
      />
    </div>
  );
};

export default SelectRoomContent;
