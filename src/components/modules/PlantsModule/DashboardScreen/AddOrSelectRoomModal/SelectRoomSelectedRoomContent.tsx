import React from "react";
import isNil from "lodash/isNil";
import { Cell, Column } from "react-table";

import InfoItem from "../../../../atoms/infoItem/InfoItem";
import Label from "../../../../atoms/label/Label";
import Table from "../../../../atoms/table/Table";
import Loader from "../../../../atoms/loader/Loader";
import { INFO_ITEM_SMALL } from "../../../../../constants/InfoItem";
import { LABEL_SMALL } from "../../../../../constants/Label";
import { PlantData, RoomData } from "../../../../../types/PlantsModule";
import { getWateringModeLabel } from "../../../../../utils/PlantsModule";
import { ListPickerItemType } from "../../../../../types/ListPicker";
import styles from "./AddOrSelectRoomModal.module.scss";
import helpers from "../../../../../styles/helpers.module.scss";

type Props = {
  isSelectedRoomFetching: boolean;
  selectedRoom: ListPickerItemType | null;
  selectedRoomData: RoomData | null;
};

const SelectRoomSelectedRoomContent: React.FC<Props> = ({
  selectedRoom,
  selectedRoomData,
  isSelectedRoomFetching,
}) => {
  const [lastMeasuredValue] = selectedRoomData?.data || [];

  const getWaterCapacityValue = () => {
    if (!isNil(lastMeasuredValue?.values?.waterCapacity)) {
      if (lastMeasuredValue?.values?.waterCapacity) {
        return "Full";
      }

      return "Empty";
    }

    return "";
  };

  const columns: Array<Column<PlantData>> = React.useMemo(
    () => [
      {
        Header: "Name",
        accessor: "name",
      },
      {
        Header: "Plant",
        accessor: "plant",
      },
      {
        Header: "Water mode",
        accessor: "mode",
        Cell: ({ value }: Cell) => getWateringModeLabel(value?.plantMode),
      },
    ],
    []
  );

  if (isSelectedRoomFetching) {
    return (
      <div className={`${styles.selectRoomSelected} ${helpers.center}`}>
        <Loader />
      </div>
    );
  }

  return (
    <div className={styles.selectRoomSelected}>
      <InfoItem
        value={selectedRoom?.label}
        label="Selected room"
        className={styles.selectRoomSelectedLabel}
      />
      <InfoItem
        value={lastMeasuredValue?.values?.temperature}
        label="Temperature"
        unit="°C"
        size={INFO_ITEM_SMALL}
      />
      <InfoItem
        value={lastMeasuredValue?.values?.humidity}
        label="Humidity"
        unit="%"
        size={INFO_ITEM_SMALL}
      />
      <InfoItem
        value={getWaterCapacityValue()}
        label="Water state"
        size={INFO_ITEM_SMALL}
      />
      <div className={styles.selectRoomTable}>
        <Label
          text="Plants"
          size={LABEL_SMALL}
          className={styles.selectRoomTableLabel}
        />
        <Table columns={columns} data={selectedRoomData?.plants || []} />
      </div>
    </div>
  );
};

export default SelectRoomSelectedRoomContent;
