import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { AxiosError, AxiosResponse } from "axios";

import Button from "../../../../atoms/button/Button";
import Input from "../../../../atoms/input/Input";
import useNotifications from "../../../../../hooks/useNotifications";
import useCheckErrorCode from "../../../../../hooks/useCheckErrorCode";
import { BUTTON_SMALL } from "../../../../../constants/Button";
import { api } from "../../../../../api/api";
import { getUserTokenSelector } from "../../../../../redux/modules/user/reducer";
import { StoreState } from "../../../../../types/StoreState";
import { getRoomAddToUserApiPath } from "../../../../../api/apiPaths";
import { plantsModuleFetchRoomsListSuccessActionCreator } from "../../../../../redux/modules/plantsModule/actions";
import {
  ERROR_NOTIF,
  SUCCESS_NOTIF,
} from "../../../../../constants/Notifications";
import {
  DEVICE_ALREADY_ASSIGNED_ERROR_CODE,
  ID_NOT_FOUND_ERROR_CODE,
} from "../../../../../constants/ErrorCodes";
import { PLANTS_MODULE } from "../../../../../constants/Services";
import styles from "./AddOrSelectRoomModal.module.scss";

const AddRoomContent: React.FC = () => {
  const dispatch = useDispatch();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const { notify } = useNotifications();
  const { checkErrorCode } = useCheckErrorCode();
  const [roomId, setRoomId] = useState<string | null>(null);
  const [isRoomAdding, setIsRoomAdding] = useState(false);

  const handleChange = (_: unknown, value: string) => setRoomId(value);

  const getErrorMessage = (errorCode: string) => {
    switch (errorCode) {
      case ID_NOT_FOUND_ERROR_CODE:
        return "Room with requested ID is not existing";
      case DEVICE_ALREADY_ASSIGNED_ERROR_CODE:
        return "Room with requested ID is already assinged to user";
      default:
        return "Error occured during adding room to user";
    }
  };

  const handleAddRoom = () => {
    setIsRoomAdding(true);

    const requestBody = {
      deviceId: roomId,
      serviceType: PLANTS_MODULE,
    };

    api(token)
      .post(getRoomAddToUserApiPath(), requestBody)
      .then((response: AxiosResponse) => {
        setIsRoomAdding(false);

        dispatch(
          plantsModuleFetchRoomsListSuccessActionCreator(
            response.data?.userRooms
          )
        );

        notify(`Room ${roomId} successfully addded`, SUCCESS_NOTIF);
        setRoomId(null);
      })
      .catch((error: AxiosError) => {
        setIsRoomAdding(false);

        const sessionErrorCode = error?.response?.data?.error;
        const [errorObject = null] = error?.response?.data?.errors || [];

        checkErrorCode(sessionErrorCode);

        if (errorObject) {
          const message = getErrorMessage(errorObject?.error);

          notify(message, ERROR_NOTIF);
        }
      });
  };

  return (
    <div className={styles.addRoomContainer}>
      <Input
        label="Device ID"
        name="TODO"
        onChange={handleChange}
        value={roomId}
        className={styles.addRoomInput}
      />
      <Button
        label="Add room"
        onClick={handleAddRoom}
        size={BUTTON_SMALL}
        disabled={!roomId || isRoomAdding}
        autoWidth
      />
    </div>
  );
};

export default AddRoomContent;
