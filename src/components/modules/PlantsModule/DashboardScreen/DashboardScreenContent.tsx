import React, { useState } from "react";
import { useSelector } from "react-redux";

import Button from "../../../atoms/button/Button";
import AddOrSelectRoomModal from "./AddOrSelectRoomModal/AddOrSelectRoomModal";
import Label from "../../../atoms/label/Label";
import ScreenHeader from "../../../molecules/screenHeader/ScreenHeader";
import Widget from "../../../templates/widget/Widget";
import StateWidget from "./widgets/StateWidget/StateWidget";
import PlantsWidget from "./widgets/PlantsWidget/PlantsWidget";
import GraphsWidget from "./widgets/GraphsWidget/GraphsWidget";
import { BUTTON_SMALL } from "../../../../constants/Button";
import { StoreState } from "../../../../types/StoreState";
import { getPlantsModuleSelectedRoomSelector } from "../../../../redux/modules/plantsModule/reducer";
import styles from "./DashboardScreenContent.module.scss";

const DashboardScreenContent: React.FC = () => {
  const [isModalVisible, setIsModalVisible] = useState(false);
  const selectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );

  return (
    <>
      <div className={styles.container}>
        <ScreenHeader title="Dashboard">
          <div className={styles.headerContainer}>
            <Label text={selectedRoom?.name} className={styles.headerLabel} />
            <Button
              label="Add or select room"
              onClick={() => setIsModalVisible(true)}
              size={BUTTON_SMALL}
            />
          </div>
        </ScreenHeader>
        <div className={styles.widgets}>
          <div className={styles.infoWidgets}>
            <div className={styles.statsWidgets}>
              <StateWidget />
              <Widget className={styles.calendarWidget} title="Calendar" />
            </div>
            <PlantsWidget />
          </div>
          <GraphsWidget />
        </div>
      </div>
      <AddOrSelectRoomModal
        visible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
      />
    </>
  );
};

export default DashboardScreenContent;
