import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import isEmpty from "lodash/isEmpty";

import DashboardScreenContent from "./DashboardScreenContent";
import Loader from "../../../atoms/loader/Loader";
import useNotifications from "../../../../hooks/useNotifications";
import useEventSourceHook from "../../../../hooks/useEventSourceHook";
import {
  plantsModuleFetchRoomsListActionCreator,
  plantsModuleFetchSelectedRoomDataActionCreator,
} from "../../../../redux/modules/plantsModule/actions";
import {
  getPlantsModuleDataDurationSelector,
  getPlantsModuleIsRoomListFetchingSelector,
  getPlantsModuleIsSelectedRoomFetchingSelector,
  getPlantsModuleRoomsListSelector,
  getPlantsModuleSelectedRoomDataSelector,
  getPlantsModuleSelectedRoomSelector,
} from "../../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../../types/StoreState";
import { getUserTokenSelector } from "../../../../redux/modules/user/reducer";

const DashboardScreenFetcher: React.FC = () => {
  const dispatch = useDispatch();
  const { notify } = useNotifications();
  const { openEventSourceEmiter } = useEventSourceHook();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const rooms = useSelector((state: StoreState) =>
    getPlantsModuleRoomsListSelector(state.plantsModule)
  );
  const isRoomsListFetching = useSelector((state: StoreState) =>
    getPlantsModuleIsRoomListFetchingSelector(state.plantsModule)
  );
  const selectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );
  const isSelectedRoomsFetching = useSelector((state: StoreState) =>
    getPlantsModuleIsSelectedRoomFetchingSelector(state.plantsModule)
  );
  const selectedRoomData = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomDataSelector(state.plantsModule)
  );
  const dataDuration = useSelector((state: StoreState) =>
    getPlantsModuleDataDurationSelector(state.plantsModule)
  );

  useEffect(() => {
    if (isEmpty(rooms)) {
      dispatch(plantsModuleFetchRoomsListActionCreator(token, notify));
    }
    // workaround for exhaustive depts - if dependency is added - rerenders for lot of times
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, rooms, token]);

  useEffect(() => {
    if (selectedRoom && !selectedRoomData) {
      dispatch(
        plantsModuleFetchSelectedRoomDataActionCreator(
          selectedRoom?.id,
          token,
          dataDuration,
          notify
        )
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dispatch, selectedRoom, selectedRoomData, token, dataDuration]);

  useEffect(() => {
    if (selectedRoom) {
      dispatch(
        plantsModuleFetchSelectedRoomDataActionCreator(
          selectedRoom?.id,
          token,
          dataDuration,
          notify
        )
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataDuration]);

  useEffect(() => {
    if (selectedRoom) {
      openEventSourceEmiter();
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [selectedRoom]);

  if (isRoomsListFetching || isSelectedRoomsFetching) {
    return <Loader />;
  }

  return <DashboardScreenContent />;
};

export default DashboardScreenFetcher;
