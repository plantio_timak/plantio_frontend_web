import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import Loader from "../../../atoms/loader/Loader";
import PlantDetailsScreenContent from "./PlantDetailsScreenContent";
import useNotifications from "../../../../hooks/useNotifications";
import { plantsModuleFetchSelectedRoomDataActionCreator } from "../../../../redux/modules/plantsModule/actions";
import {
  getPlantsModuleDataDurationSelector,
  getPlantsModuleIsSelectedRoomFetchingSelector,
  getPlantsModuleSelectedRoomSelector,
} from "../../../../redux/modules/plantsModule/reducer";
import { getUserTokenSelector } from "../../../../redux/modules/user/reducer";
import { StoreState } from "../../../../types/StoreState";

const PlantDetailsScreenFetcher: React.FC = () => {
  const dispatch = useDispatch();
  const { notify } = useNotifications();

  const dataDuration = useSelector((state: StoreState) =>
    getPlantsModuleDataDurationSelector(state.plantsModule)
  );
  const selectedRoom = useSelector((state: StoreState) =>
    getPlantsModuleSelectedRoomSelector(state.plantsModule)
  );
  const isSelectedRoomDataFetching = useSelector((state: StoreState) =>
    getPlantsModuleIsSelectedRoomFetchingSelector(state.plantsModule)
  );
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );

  useEffect(() => {
    if (selectedRoom) {
      dispatch(
        plantsModuleFetchSelectedRoomDataActionCreator(
          selectedRoom?.id,
          token,
          dataDuration,
          notify
        )
      );
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [dataDuration, selectedRoom, token, dispatch]);

  if (isSelectedRoomDataFetching) {
    return <Loader />;
  }

  return <PlantDetailsScreenContent />;
};

export default PlantDetailsScreenFetcher;
