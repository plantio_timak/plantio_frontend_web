import React from "react";
import { useSelector } from "react-redux";

import InfoItem from "../../../../atoms/infoItem/InfoItem";
import { ListPickerItemType } from "../../../../../types/ListPicker";
import { INFO_ITEM_SMALL } from "../../../../../constants/InfoItem";
import { StoreState } from "../../../../../types/StoreState";
import { getPlantsModuleSinglePlantDataSelector } from "../../../../../redux/modules/plantsModule/reducer";
import {
  getLastPlantValueProperty,
  getWateringModeLabel,
} from "../../../../../utils/PlantsModule";
import { PlantMode } from "../../../../../types/PlantsModule";
import styles from "./SelectPlantModal.module.scss";

type Props = {
  selectedPlant: ListPickerItemType | null;
};

const SelectPlantSelectedPlantContent: React.FC<Props> = ({
  selectedPlant,
}) => {
  const selectedPlantData = useSelector((state: StoreState) =>
    getPlantsModuleSinglePlantDataSelector(
      state.plantsModule,
      selectedPlant?.value as number
    )
  );

  return (
    <div className={styles.selectPlantSelected}>
      <InfoItem
        value={selectedPlant?.label}
        label="Selected plant"
        className={styles.selectPlantSelectedLabel}
      />
      <InfoItem
        value={selectedPlantData?.plant}
        label="Plant"
        size={INFO_ITEM_SMALL}
      />
      <InfoItem
        value={getWateringModeLabel(
          selectedPlantData?.mode?.plantMode as PlantMode
        )}
        label="Plant mode"
        size={INFO_ITEM_SMALL}
      />
      <InfoItem
        value={getLastPlantValueProperty(
          selectedPlantData?.plantValues,
          "temperature"
        )}
        label="Soil temperature"
        unit="°C"
        size={INFO_ITEM_SMALL}
      />
      <InfoItem
        value={getLastPlantValueProperty(
          selectedPlantData?.plantValues,
          "humidity"
        )}
        label="Soil humidity"
        unit="%"
        size={INFO_ITEM_SMALL}
      />
    </div>
  );
};

export default SelectPlantSelectedPlantContent;
