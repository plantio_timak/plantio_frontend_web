import React, { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { useSelector } from "react-redux";

import Button from "../../../../atoms/button/Button";
import Modal from "../../../../templates/modal/Modal";
import SelectPlantContent from "./SelectPlantContent";
import useNotifications from "../../../../../hooks/useNotifications";
import {
  BUTTON_SECONDARY,
  BUTTON_SMALL,
} from "../../../../../constants/Button";
import { getPlantDetailsRoutePath } from "../../../../../utils/Screens";
import { SUCCESS_NOTIF } from "../../../../../constants/Notifications";
import { ListPickerItemType } from "../../../../../types/ListPicker";
import { StoreState } from "../../../../../types/StoreState";
import { getPlantsModulePlantsDataSelector } from "../../../../../redux/modules/plantsModule/reducer";

type RouteParams = {
  id: string;
};

type Props = {
  visible: boolean;
  setIsModalVisible: (isVisible: boolean) => void;
};

const SelectPlantModal: React.FC<Props> = ({ visible, setIsModalVisible }) => {
  const { id } = useParams<RouteParams>();
  const history = useHistory();
  const { notify } = useNotifications();

  const plants = useSelector((state: StoreState) =>
    getPlantsModulePlantsDataSelector(state.plantsModule)
  );

  const [selectedPlant, setSelectedPlant] = useState<ListPickerItemType | null>(
    null
  );

  useEffect(() => {
    const plantData = plants?.find((plant) => plant.id === parseInt(id));

    if (plantData) {
      setSelectedPlant({
        value: plantData?.id,
        label: plantData?.name,
      });
    }
  }, [plants, id]);

  const handleConfirm = () => {
    setIsModalVisible(false);
    notify(
      `Plant was changed. Selected plant: ${selectedPlant?.label}`,
      SUCCESS_NOTIF
    );
    history.push(getPlantDetailsRoutePath(selectedPlant?.value as number));
  };

  const handleCancel = () => {
    const plantData = plants?.find((plant) => plant.id === parseInt(id));

    if (plantData) {
      setSelectedPlant({
        value: plantData?.id,
        label: plantData?.name,
      });
    }
    setIsModalVisible(false);
  };

  return (
    <Modal visible={visible}>
      <Modal.Header label="Select plant" />
      <Modal.Body>
        <SelectPlantContent
          plants={plants}
          selectedPlant={selectedPlant}
          setSelectedPlant={setSelectedPlant}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button
          label="Confirm"
          onClick={handleConfirm}
          size={BUTTON_SMALL}
          disabled={!selectedPlant}
        />
        <Button
          label="Cancel"
          onClick={handleCancel}
          size={BUTTON_SMALL}
          type={BUTTON_SECONDARY}
        />
      </Modal.Footer>
    </Modal>
  );
};

export default SelectPlantModal;
