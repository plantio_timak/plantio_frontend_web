import React from "react";

import SelectPlantSelectedPlantContent from "./SelectPlantSelectedPlantContent";
import ListPicker from "../../../../atoms/listPicker/ListPicker";
import { ListPickerItemType } from "../../../../../types/ListPicker";
import { PlantData } from "../../../../../types/PlantsModule";
import styles from "./SelectPlantModal.module.scss";

type Props = {
  plants: Array<PlantData>;
  selectedPlant: ListPickerItemType | null;
  setSelectedPlant: (value: ListPickerItemType) => void;
};

const SelectPlantContent: React.FC<Props> = ({
  plants,
  selectedPlant,
  setSelectedPlant,
}) => {
  const getListPickerItems = (): Array<ListPickerItemType> =>
    plants?.map((plant) => ({
      value: plant.id,
      label: plant.name,
    }));

  const pickerItems = getListPickerItems();

  return (
    <div className={styles.selectPlantContainer}>
      <div className={styles.selectPlantPicker}>
        <ListPicker
          items={pickerItems}
          selectedItem={selectedPlant}
          onItemClick={(value) => setSelectedPlant(value)}
        />
      </div>
      <SelectPlantSelectedPlantContent selectedPlant={selectedPlant} />
    </div>
  );
};

export default SelectPlantContent;
