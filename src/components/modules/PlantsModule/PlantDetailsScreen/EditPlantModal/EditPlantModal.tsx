import React, { useEffect } from "react";
import isEmpty from "lodash/isEmpty";
import { useForm } from "react-hook-form";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { AxiosError, AxiosResponse } from "axios";

import Button from "../../../../atoms/button/Button";
import Modal from "../../../../templates/modal/Modal";
import Input from "../../../../atoms/input/Input";
import useCheckErrorCode from "../../../../../hooks/useCheckErrorCode";
import useNotifications from "../../../../../hooks/useNotifications";
import {
  BUTTON_SECONDARY,
  BUTTON_SMALL,
} from "../../../../../constants/Button";
import { EditPlantParams } from "../../../../../types/PlantDetailsScreenTypes";
import { onChangeHandler } from "../../../../../utils/Form";
import { StoreState } from "../../../../../types/StoreState";
import { getPlantsModuleSinglePlantDataSelector } from "../../../../../redux/modules/plantsModule/reducer";
import { api } from "../../../../../api/api";
import { getUserTokenSelector } from "../../../../../redux/modules/user/reducer";
import { getPlantDataEditApiPath } from "../../../../../api/apiPaths";
import { plantsModuleEditPlantDataActionCreator } from "../../../../../redux/modules/plantsModule/actions";
import {
  ERROR_NOTIF,
  SUCCESS_NOTIF,
} from "../../../../../constants/Notifications";
import styles from "./EditPlantModal.module.scss";

type RouteParams = {
  id: string;
};

type Props = {
  visible: boolean;
  setIsModalVisible: (isVisible: boolean) => void;
};

const EditPlantModal: React.FC<Props> = ({ visible, setIsModalVisible }) => {
  const { notify } = useNotifications();
  const { checkErrorCode } = useCheckErrorCode();
  const dispatch = useDispatch();
  const { id } = useParams<RouteParams>();

  const plantData = useSelector((state: StoreState) =>
    getPlantsModuleSinglePlantDataSelector(state.plantsModule, parseInt(id))
  );
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );

  const {
    register,
    handleSubmit,
    errors,
    setValue,
    watch,
    reset,
  } = useForm<EditPlantParams>({
    defaultValues: {
      name: plantData?.name,
      plant: plantData?.plant,
    },
  });

  useEffect(() => {
    register(
      { name: "name" },
      {
        required: "Name is required field",
        maxLength: {
          value: 40,
          message: "Max allowed amount of chars is 40",
        },
      }
    );
    register(
      { name: "plant" },
      {
        maxLength: {
          value: 40,
          message: "Max allowed amount of chars is 40",
        },
      }
    );
  }, [register]);

  useEffect(() => {
    reset({
      name: plantData?.name,
      plant: plantData?.plant,
    });
  }, [plantData, reset]);

  const onSubmit = (values: EditPlantParams) => {
    api(token)
      .put(getPlantDataEditApiPath(parseInt(id)), values)
      .then((response: AxiosResponse) => {
        dispatch(plantsModuleEditPlantDataActionCreator(response.data));
        notify("Successfully edited plant information", SUCCESS_NOTIF);
        setIsModalVisible(false);
      })
      .catch((error: AxiosError) => {
        const sessionErrorCode = error?.response?.data?.error;
        const [errorObject = null] = error?.response?.data?.errors || [];

        checkErrorCode(sessionErrorCode);

        if (errorObject) {
          notify("Error occured during plant editing", ERROR_NOTIF);
        }
      });
  };

  const onCancel = () => {
    setIsModalVisible(false);
    reset();
  };

  const { name, plant } = watch();

  return (
    <Modal visible={visible} autoWidth>
      <Modal.Header label="Edit plant" />
      <Modal.Body>
        <div className={styles.form}>
          <Input
            name="name"
            label="Name"
            error={errors?.name?.message}
            value={name}
            onChange={onChangeHandler(setValue)}
          />
          <Input
            name="plant"
            label="Plant"
            error={errors?.plant?.message}
            value={plant}
            onChange={onChangeHandler(setValue)}
          />
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          label="Confirm"
          onClick={handleSubmit(onSubmit)}
          size={BUTTON_SMALL}
          disabled={!isEmpty(errors)}
        />
        <Button
          label="Cancel"
          onClick={onCancel}
          size={BUTTON_SMALL}
          type={BUTTON_SECONDARY}
        />
      </Modal.Footer>
    </Modal>
  );
};

export default EditPlantModal;
