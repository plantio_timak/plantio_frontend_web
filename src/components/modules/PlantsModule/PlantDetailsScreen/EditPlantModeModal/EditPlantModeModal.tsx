import React, { useEffect } from "react";
import isEmpty from "lodash/isEmpty";
import { useParams } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { useForm } from "react-hook-form";
import { AxiosError, AxiosResponse } from "axios";

import Button from "../../../../atoms/button/Button";
import Modal from "../../../../templates/modal/Modal";
import Select from "../../../../atoms/select/Select";
import Input from "../../../../atoms/input/Input";
import useNotifications from "../../../../../hooks/useNotifications";
import useCheckErrorCode from "../../../../../hooks/useCheckErrorCode";
import {
  BUTTON_SECONDARY,
  BUTTON_SMALL,
} from "../../../../../constants/Button";
import {
  PLANT_MODES,
  PLANT_MODE_INTERACTIVE,
  PLANT_MODE_TIME,
} from "../../../../../constants/PlantsModule";
import { getWateringModeLabel } from "../../../../../utils/PlantsModule";
import { PlantMode } from "../../../../../types/PlantsModule";
import { StoreState } from "../../../../../types/StoreState";
import {
  getPlantsModuleSinglePlantDataSelector,
  getPlantsModuleWateringModesSelector,
} from "../../../../../redux/modules/plantsModule/reducer";
import { EditPlantModeParams } from "../../../../../types/PlantDetailsScreenTypes";
import { onChangeHandler } from "../../../../../utils/Form";
import { api } from "../../../../../api/api";
import { getUserTokenSelector } from "../../../../../redux/modules/user/reducer";
import { getPlantModeEditApiPath } from "../../../../../api/apiPaths";
import { plantsModuleEditPlantDataActionCreator } from "../../../../../redux/modules/plantsModule/actions";
import {
  ERROR_NOTIF,
  SUCCESS_NOTIF,
} from "../../../../../constants/Notifications";
import styles from "./EditPlantModeModal.module.scss";

type RouteParams = {
  id: string;
};

type Props = {
  visible: boolean;
  setIsModalVisible: (visible: boolean) => void;
};

const EditPlantModeModal: React.FC<Props> = ({
  visible,
  setIsModalVisible,
}) => {
  const dispatch = useDispatch();
  const { id } = useParams<RouteParams>();
  const { notify } = useNotifications();
  const { checkErrorCode } = useCheckErrorCode();

  const plantData = useSelector((state: StoreState) =>
    getPlantsModuleSinglePlantDataSelector(state.plantsModule, parseInt(id))
  );
  const wateringModes = useSelector((state: StoreState) =>
    getPlantsModuleWateringModesSelector(state.plantsModule)
  );
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );

  const {
    register,
    handleSubmit,
    setValue,
    watch,
    reset,
    errors,
    clearErrors,
    trigger,
  } = useForm<EditPlantModeParams>({
    defaultValues: {
      plantMode: plantData?.mode.plantMode,
      time: plantData?.mode?.time,
      wateringModeId: plantData?.wateringMode?.id,
    },
  });

  const { wateringModeId, plantMode, time } = watch();

  useEffect(() => {
    register({ name: "plantMode" });
    register(
      { name: "time" },
      {
        validate: (value) => {
          const isRequired = plantMode === PLANT_MODE_TIME && !value;

          if (isRequired) {
            return "Time is required field";
          }

          return undefined;
        },
      }
    );
    register(
      { name: "wateringModeId" },
      {
        validate: (value) => {
          const isRequired = plantMode === PLANT_MODE_INTERACTIVE && !value;

          if (isRequired) {
            return "Watering mode is required field";
          }

          return undefined;
        },
      }
    );
  }, [register, plantMode]);

  useEffect(() => {
    if (wateringModeId) {
      trigger("wateringModeId");
    }
  }, [wateringModeId, trigger]);

  useEffect(() => {
    clearErrors();

    if (plantMode === PLANT_MODE_TIME) {
      setValue("wateringModeId", null);
      setValue("time", plantData?.mode?.time);
    } else {
      setValue("time", null);
      setValue("wateringModeId", plantData?.wateringMode?.id);
    }
  }, [plantMode, setValue, plantData, clearErrors]);

  useEffect(() => {
    reset({
      plantMode: plantData?.mode.plantMode,
      time: plantData?.mode?.time,
      wateringModeId: plantData?.wateringMode?.id,
    });
  }, [plantData, reset]);

  const getPlantModeOptions = () =>
    PLANT_MODES.map((mode) => ({
      value: mode,
      label: getWateringModeLabel(mode as PlantMode),
    }));

  const getWateringModeOptions = () =>
    wateringModes?.map((mode) => ({
      value: mode.id,
      label: mode.name,
    }));

  const plantModeOptions = getPlantModeOptions();
  const wateringModeOptions = getWateringModeOptions();

  const onSelectChanged = (id: "wateringModeId" | "plantMode") => (
    value: string | number
  ) => {
    setValue(id, value);
  };

  const onSubmit = (values: EditPlantModeParams) => {
    api(token)
      .put(getPlantModeEditApiPath(parseInt(id)), values)
      .then((response: AxiosResponse) => {
        dispatch(plantsModuleEditPlantDataActionCreator(response.data));
        notify("Successfully edited plant mode", SUCCESS_NOTIF);
        setIsModalVisible(false);
      })
      .catch((error: AxiosError) => {
        const sessionErrorCode = error?.response?.data?.error;
        const [errorObject = null] = error?.response?.data?.errors || [];

        checkErrorCode(sessionErrorCode);

        if (errorObject) {
          notify("Error occured during plant mode editing", ERROR_NOTIF);
        }
      });
  };

  const renderModeField = () => {
    if (plantMode === PLANT_MODE_TIME) {
      return (
        <Input
          label="Time"
          name="time"
          value={time}
          error={errors?.time?.message as string}
          onChange={onChangeHandler(setValue)}
        />
      );
    }

    return (
      <Select
        label="Watering mode"
        options={wateringModeOptions}
        value={wateringModeId}
        error={errors?.wateringModeId?.message as string}
        onChange={onSelectChanged("wateringModeId")}
      />
    );
  };

  return (
    <Modal visible={visible} autoWidth>
      <Modal.Header label="Edit plant mode" />
      <Modal.Body>
        <div className={styles.form}>
          <Select
            label="Mode"
            options={plantModeOptions}
            onChange={onSelectChanged("plantMode")}
            value={plantMode}
          />
          {renderModeField()}
        </div>
      </Modal.Body>
      <Modal.Footer>
        <Button
          label="Confirm"
          onClick={handleSubmit(onSubmit)}
          size={BUTTON_SMALL}
          disabled={!isEmpty(errors)}
        />
        <Button
          label="Cancel"
          onClick={() => setIsModalVisible(false)}
          size={BUTTON_SMALL}
          type={BUTTON_SECONDARY}
        />
        r
      </Modal.Footer>
    </Modal>
  );
};

export default EditPlantModeModal;
