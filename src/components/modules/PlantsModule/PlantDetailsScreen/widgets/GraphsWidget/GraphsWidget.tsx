import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router";

import LineChart from "../../../../../atoms/lineChart/LineChart";
import Widget from "../../../../../templates/widget/Widget";
import { getPlantsModulePlantsDataSelector } from "../../../../../../redux/modules/plantsModule/reducer";
import { PlantData } from "../../../../../../types/PlantsModule";
import { StoreState } from "../../../../../../types/StoreState";
import styles from "./GraphsWidget.module.scss";

type RouteParams = {
  id: string;
};

const GraphsWidget: React.FC = () => {
  const { id } = useParams<RouteParams>();

  const plantsData = useSelector((state: StoreState) =>
    getPlantsModulePlantsDataSelector(state.plantsModule)
  );

  const getMeasurementsData = (time: string, key: string) => {
    const items: Record<string, number | null> = {};

    plantsData?.forEach((item) => {
      const valuesToUse = item.plantValues.find((value) => {
        const currentTime = value.time.split(".");
        const timeToCompare = time.split(".");

        return currentTime[0].slice(0, -2) === timeToCompare[0].slice(0, -2);
      });

      const values = valuesToUse?.values as Record<string, number>;

      if (values) {
        items[`${item.id}_${key}`] = values[key];
      } else {
        items[`${item.id}_${key}`] = null;
      }
    });

    return items;
  };

  const getMeasurements = (data: Array<PlantData>, key: string) => {
    const mainData = data.find((item) => item.id === parseInt(id));

    return (
      mainData?.plantValues?.map((measurement) => {
        const date = new Date(measurement?.time);

        return {
          time: date,
          ...getMeasurementsData(measurement?.time, key),
        };
      }) || []
    );
  };

  const getDataKeys = (value: string, label: string) => {
    return plantsData.map((item) => {
      return {
        label: `${item.name} ${label}`,
        value: `${item.id}_${value}`,
        defaultVisible: item.id === parseInt(id),
      };
    });
  };

  const tempData = {
    keys: getDataKeys("temperature", "Temperature"),
    data: getMeasurements(plantsData, "temperature").reverse(),
  };

  const humData = {
    keys: getDataKeys("humidity", "Humidity"),
    data: getMeasurements(plantsData, "humidity").reverse(),
  };

  return (
    <Widget title="Graphs" className={styles.container}>
      <div className={styles.content}>
        <LineChart
          data={tempData}
          xLabel="Time"
          yLabel="Temp"
          xKey="time"
          yUnit="°C"
        />
        <LineChart
          data={humData}
          xLabel="Time"
          yLabel="Humidity"
          xKey="time"
          yUnit="%"
        />
      </div>
    </Widget>
  );
};

export default GraphsWidget;
