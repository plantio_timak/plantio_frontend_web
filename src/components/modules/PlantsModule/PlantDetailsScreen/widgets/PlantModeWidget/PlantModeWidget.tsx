import React from "react";
import { useParams } from "react-router-dom";
import { useSelector } from "react-redux";

import Widget from "../../../../../templates/widget/Widget";
import Label from "../../../../../atoms/label/Label";
import InfoItem from "../../../../../atoms/infoItem/InfoItem";
import { StoreState } from "../../../../../../types/StoreState";
import { getPlantsModuleSinglePlantDataSelector } from "../../../../../../redux/modules/plantsModule/reducer";
import {
  getWateringModeDescription,
  getWateringModeLabel,
} from "../../../../../../utils/PlantsModule";
import { PlantMode } from "../../../../../../types/PlantsModule";
import { EDIT_ICON } from "../../../../../../constants/Icon";
import { PLANT_MODE_TIME } from "../../../../../../constants/PlantsModule";
import { LABEL_NORMAL } from "../../../../../../constants/Label";
import styles from "./PlantModeWidget.module.scss";

type RouteParams = {
  id: string;
};

type Props = {
  onEditPlantModeClicked: () => void;
};

const PlantModeWidget: React.FC<Props> = ({ onEditPlantModeClicked }) => {
  const { id } = useParams<RouteParams>();
  const plantData = useSelector((state: StoreState) =>
    getPlantsModuleSinglePlantDataSelector(state.plantsModule, parseInt(id))
  );

  const renderModeItems = () => {
    if (plantData?.mode?.plantMode === PLANT_MODE_TIME) {
      return <InfoItem label="Time" value={plantData?.mode?.time} unit="h" />;
    }

    return <InfoItem label="Mode" value={plantData?.wateringMode?.name} />;
  };

  const description = getWateringModeDescription(
    plantData?.mode?.plantMode,
    plantData?.mode?.time,
    plantData?.wateringMode
  );

  return (
    <Widget
      className={styles.container}
      title="Plant mode"
      icon={EDIT_ICON}
      onIconClick={onEditPlantModeClicked}
    >
      <div className={styles.content}>
        <div className={styles.items}>
          <InfoItem
            label="Plant mode"
            value={getWateringModeLabel(
              plantData?.mode?.plantMode as PlantMode
            )}
          />
          {renderModeItems()}
        </div>
        <div className={styles.desc}>
          <Label text={description} size={LABEL_NORMAL} />
        </div>
      </div>
    </Widget>
  );
};

export default PlantModeWidget;
