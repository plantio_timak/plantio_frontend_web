import React from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import Widget from "../../../../../templates/widget/Widget";
import InfoItem from "../../../../../atoms/infoItem/InfoItem";
import { getPlantsModuleSinglePlantDataSelector } from "../../../../../../redux/modules/plantsModule/reducer";
import { StoreState } from "../../../../../../types/StoreState";
import { EDIT_ICON } from "../../../../../../constants/Icon";
import styles from "./PlantWidget.module.scss";
import { getLastPlantValueProperty } from "../../../../../../utils/PlantsModule";

type RouteParams = {
  id: string;
};

type Props = {
  onEditPlantClicked: () => void;
};

const PlantWidget: React.FC<Props> = ({ onEditPlantClicked }) => {
  const { id } = useParams<RouteParams>();

  const plantData = useSelector((state: StoreState) =>
    getPlantsModuleSinglePlantDataSelector(state.plantsModule, parseInt(id))
  );

  return (
    <Widget
      className={styles.container}
      title="Plant"
      icon={EDIT_ICON}
      onIconClick={onEditPlantClicked}
    >
      <InfoItem label="Plant" value={plantData?.plant} />
      <InfoItem
        label="Soil temperature"
        value={getLastPlantValueProperty(plantData?.plantValues, "temperature")}
        unit="°C"
      />
      <InfoItem
        label="Soil humidity"
        value={getLastPlantValueProperty(plantData?.plantValues, "humidity")}
        unit="%"
      />
    </Widget>
  );
};

export default PlantWidget;
