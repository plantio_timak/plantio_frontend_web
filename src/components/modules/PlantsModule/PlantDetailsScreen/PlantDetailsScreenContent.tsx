import React, { useState } from "react";
import { useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import Button from "../../../atoms/button/Button";
import Label from "../../../atoms/label/Label";
import ScreenHeader from "../../../molecules/screenHeader/ScreenHeader";
import Widget from "../../../templates/widget/Widget";
import SelectPlantModal from "./SelectPlantModal/SelectPlantModal";
import PlantWidget from "./widgets/PlantWidget/PlantWidget";
import GraphsWidget from "./widgets/GraphsWidget/GraphsWidget";
import PlantModeWidget from "./widgets/PlantModeWidget/PlantModeWidget";
import EditPlantModal from "./EditPlantModal/EditPlantModal";
import EditPlantModeModal from "./EditPlantModeModal/EditPlantModeModal";
import { BUTTON_SMALL } from "../../../../constants/Button";
import { StoreState } from "../../../../types/StoreState";
import { getPlantsModuleSinglePlantDataSelector } from "../../../../redux/modules/plantsModule/reducer";
import styles from "./PlantDetailsScreenContent.module.scss";

type RouteParams = {
  id: string;
};

const PlantDetailsScreenContent: React.FC = () => {
  const { id } = useParams<RouteParams>();

  const plant = useSelector((state: StoreState) =>
    getPlantsModuleSinglePlantDataSelector(state.plantsModule, parseInt(id))
  );

  const [isSelectPlantModalVisible, setIsSelectPlantModalVisible] = useState(
    false
  );
  const [isEditPlantModalVisible, setIsEditPlantModalVisible] = useState(false);
  const [
    isEditPlantModeModalVisible,
    setIsEditPlantModeModalVisible,
  ] = useState(false);

  const renderContent = () => {
    if (!plant) {
      return null;
    }

    return (
      <div className={styles.widgets}>
        <div className={styles.infoWidgets}>
          <div className={styles.statsWidgets}>
            <PlantWidget
              onEditPlantClicked={() => setIsEditPlantModalVisible(true)}
            />
            <Widget className={styles.todoWidget} title="TODO" />
          </div>
          <PlantModeWidget
            onEditPlantModeClicked={() => setIsEditPlantModeModalVisible(true)}
          />
        </div>
        <GraphsWidget />
      </div>
    );
  };

  return (
    <>
      <div className={styles.container}>
        <ScreenHeader title="Plant details" hasBackButton>
          <div className={styles.headerContainer}>
            <Label text={plant?.name} className={styles.headerLabel} />
            <Button
              label="Select plant"
              onClick={() => setIsSelectPlantModalVisible(true)}
              size={BUTTON_SMALL}
            />
          </div>
        </ScreenHeader>
        {renderContent()}
      </div>
      <SelectPlantModal
        visible={isSelectPlantModalVisible}
        setIsModalVisible={setIsSelectPlantModalVisible}
      />
      <EditPlantModal
        visible={isEditPlantModalVisible}
        setIsModalVisible={setIsEditPlantModalVisible}
      />
      <EditPlantModeModal
        visible={isEditPlantModeModalVisible}
        setIsModalVisible={setIsEditPlantModeModalVisible}
      />
    </>
  );
};

export default PlantDetailsScreenContent;
