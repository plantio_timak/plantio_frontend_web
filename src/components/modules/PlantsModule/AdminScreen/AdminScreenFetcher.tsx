import React from "react";

import AdminScreenContent from "./AdminScreenContent";
import Screen from "../../../templates/screen/Screen";

const AdminScreenFetcher: React.FC = () => (
  <Screen showNavigation={false}>
    <AdminScreenContent />
  </Screen>
);

export default AdminScreenFetcher;
