import React, { useEffect } from "react";
import { useForm } from "react-hook-form";

import useNotifications from "../../../../hooks/useNotifications";
import { SUCCESS_NOTIF } from "../../../../constants/Notifications";
import Modal from "../../../templates/modal/Modal";
import Button from "../../../atoms/button/Button";
import { BUTTON_SECONDARY, BUTTON_SMALL } from "../../../../constants/Button";
import Input from "../../../atoms/input/Input";
import { Device } from "../../../../types/Device";
import { onChangeHandler } from "../../../../utils/Form";

type Props = {
  visible: boolean;
  setIsModalVisible: (visible: boolean) => void;
  append: (obj: Device) => void;
};

const AdminAddingDeviceModal: React.FC<Props> = ({
  visible,
  setIsModalVisible,
  append,
}) => {
  const {
    register,
    errors,
    watch,
    setValue,
    handleSubmit,
    reset,
  } = useForm<Device>();
  const { notify } = useNotifications();

  const handleConfirmAddedDevice = (device: Device) => {
    notify(`Device was added to form`, SUCCESS_NOTIF);
    setIsModalVisible(false);
    append(device);
    reset();
  };

  useEffect(() => {
    register(
      { name: "id" },
      {
        required: "Device Id is required field",
      }
    );
    register(
      { name: "type" },
      {
        required: "Device name is required field",
      }
    );
  }, [register]);

  const { id, type } = watch();

  return (
    <Modal visible={visible}>
      <Modal.Header label="Add device" />
      <Modal.Body>
        <Input
          label="Id"
          name="id"
          error={errors?.id?.message}
          onChange={onChangeHandler(setValue)}
          value={id}
        />
        <Input
          name="type"
          label="Name"
          error={errors?.type?.message}
          onChange={onChangeHandler(setValue)}
          value={type}
        />
      </Modal.Body>
      <Modal.Footer>
        <Button
          label="Confirm"
          onClick={handleSubmit(handleConfirmAddedDevice)}
          size={BUTTON_SMALL}
        />
        <Button
          label="Cancel"
          onClick={() => setIsModalVisible(false)}
          size={BUTTON_SMALL}
          type={BUTTON_SECONDARY}
        />
      </Modal.Footer>
    </Modal>
  );
};

export default AdminAddingDeviceModal;
