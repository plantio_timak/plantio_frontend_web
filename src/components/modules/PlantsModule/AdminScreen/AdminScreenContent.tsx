import React, { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { isEmpty } from "lodash";
import { useSelector } from "react-redux";
import { AxiosError } from "axios";

import {
  getUserIsFetchingSelector,
  getUserTokenSelector,
} from "../../../../redux/modules/user/reducer";
import { StoreState } from "../../../../types/StoreState";
import Form from "../../../templates/form/Form";
import Input from "../../../atoms/input/Input";
import { onChangeHandler } from "../../../../utils/Form";
import { AdminParams } from "../../../../types/AdminScreenTypes";
import { api } from "../../../../api/api";
import { getCreateRoomForUserApiPath } from "../../../../api/apiPaths";
import {
  ERROR_NOTIF,
  SUCCESS_NOTIF,
} from "../../../../constants/Notifications";
import useNotifications from "../../../../hooks/useNotifications";
import useCheckErrorCode from "../../../../hooks/useCheckErrorCode";
import {
  DEVICE_ALREADY_ASSIGNED_ERROR_CODE,
  FIELD_INVALID_ERROR_CODE,
  ROOM_ALREADY_EXISTS_ERROR_CODE,
} from "../../../../constants/ErrorCodes";
import Button from "../../../atoms/button/Button";
import { BUTTON_SMALL } from "../../../../constants/Button";
import AdminAddingDeviceModal from "./AdminAddingDeviceModal";
import { PLANTS_MODULE } from "../../../../constants/Services";
import { Device } from "../../../../types/Device";

const AdminScreenContent: React.FC = () => {
  const {
    register,
    handleSubmit,
    errors,
    setValue,
    watch,
    reset,
  } = useForm<AdminParams>();

  const { notify } = useNotifications();
  const token = useSelector((state: StoreState) =>
    getUserTokenSelector(state.user)
  );
  const isFetching = useSelector((state: StoreState) =>
    getUserIsFetchingSelector(state.user)
  );
  const { checkErrorCode } = useCheckErrorCode();
  const [isModalVisible, setIsModalVisible] = useState(false);
  const [devices, setDevices] = useState<Array<Device>>([]);

  useEffect(() => {
    register(
      { name: "id" },
      {
        required: "Id is required field",
      }
    );
    register(
      { name: "ip" },
      {
        required: "Ip is required field",
      }
    );
    register(
      { name: "name" },
      {
        required: "Name is required field",
      }
    );
  }, [register]);

  const getErrorMessage = (errorCode: string) => {
    switch (errorCode) {
      case FIELD_INVALID_ERROR_CODE:
        return "Room with requested ID already existing";
      case ROOM_ALREADY_EXISTS_ERROR_CODE:
        return "Room with requested ID already existing";
      case DEVICE_ALREADY_ASSIGNED_ERROR_CODE:
        return "Device with requested ID is already assinged to user";
      default:
        return "Error occured during adding room to user";
    }
  };

  const addDevice = (newDevice: Device) => {
    setDevices([...devices, newDevice]);
  };

  const onSubmit = (values: AdminParams) => {
    const requestBody = {
      serviceType: PLANTS_MODULE,
      id: values.id,
      ip: values.ip,
      name: values.name,
      devices,
    };

    api(token)
      .post(getCreateRoomForUserApiPath(), requestBody)
      .then(() => {
        //TODO reset values in form
        setDevices([]);
        notify(`Room successfully created`, SUCCESS_NOTIF);
        reset();
      })
      .catch((error: AxiosError) => {
        const sessionErrorCode = error?.response?.data?.error;
        const [errorObject = null] = error?.response?.data?.errors || [];

        checkErrorCode(sessionErrorCode);
        setDevices([]);
        if (errorObject) {
          const message = getErrorMessage(errorObject?.error);

          notify(message, ERROR_NOTIF);
        }
      });
  };

  const { id, ip, name } = watch();

  return (
    <>
      <Form
        label="Create room and device"
        buttonLabel="Confirm"
        onSubmit={handleSubmit(onSubmit)}
        submitDisabled={!isEmpty(errors) || isFetching}
      >
        <Input
          name="id"
          label="Id"
          onChange={onChangeHandler(setValue)}
          error={errors?.id?.message}
          value={id}
        />
        <Input
          label="IP"
          name="ip"
          onChange={onChangeHandler(setValue)}
          error={errors?.ip?.message}
          value={ip}
        />
        <Input
          label="Name"
          name="name"
          onChange={onChangeHandler(setValue)}
          error={errors?.name?.message}
          value={name}
        />
        <Button
          label="Add device"
          onClick={() => setIsModalVisible(true)}
          size={BUTTON_SMALL}
          autoWidth
        />
      </Form>

      <AdminAddingDeviceModal
        visible={isModalVisible}
        setIsModalVisible={setIsModalVisible}
        append={addDevice}
      />
    </>
  );
};

export default AdminScreenContent;
