import {
  PLANTS_MODULE_DURATION_DAY,
  PLANTS_MODULE_DURATION_MONTH,
  PLANTS_MODULE_DURATION_WEEK,
  PLANT_MODE_INTERACTIVE,
  PLANT_MODE_TIME,
  REAL_TIME_PLANT_TOPIC,
  REAL_TIME_ROOM_TOPIC,
} from "../constants/PlantsModule";

export type RoomSensorInfo = {
  humidity: number;
  lightIntensity: number;
  temperature: number;
  waterCapacity: boolean;
};

export type PlantSensorInfo = {
  humidity: number;
  temperature: number;
};

export type PlantMode = typeof PLANT_MODE_TIME | typeof PLANT_MODE_INTERACTIVE;

export type WateringMode = {
  id: number;
  name: string;
};

export type PlantData = {
  id: number;
  name: string;
  plant: string;
  mode: {
    plantMode: PlantMode;
    time?: string;
  };
  wateringMode?: WateringMode;
  plantValues: Array<PlantDataApi>;
};

export type PlantDataApi = {
  time: string;
  topic: string;
  values: PlantSensorInfo;
};

export type RoomDataApi = {
  time: string;
  topic: string;
  values: RoomSensorInfo;
};

export type RoomPlantsDataApi = {
  plants: Array<PlantData>;
};

export type Room = {
  id: number;
  name: string;
};

export type PlantsModuleRealTimeTopic =
  | typeof REAL_TIME_ROOM_TOPIC
  | typeof REAL_TIME_PLANT_TOPIC;

export type PlantsModuleRealTimeApi = {
  measurement: PlantsModuleRealTimeTopic;
  time: string;
  fields: PlantsModulePlantRealTimeFields | PlantsModuleRoomRealTimeFields;
};

export type PlantsModulePlantRealTimeFields = {
  plant_id: string;
  temperature: string;
  humidity: string;
};

export type PlantsModuleRoomRealTimeFields = {
  room_id: string;
  temperature: string;
  humidity: string;
  light_intensity: string;
};
export type RoomData = { data: Array<RoomDataApi> } & RoomPlantsDataApi;

export type PlantsModuleDataDuration =
  | typeof PLANTS_MODULE_DURATION_DAY
  | typeof PLANTS_MODULE_DURATION_WEEK
  | typeof PLANTS_MODULE_DURATION_MONTH;
