export type Device = {
  id: string | null;
  type: string | null;
};
