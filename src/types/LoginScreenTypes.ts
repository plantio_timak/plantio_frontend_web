export type LoginParams = {
  userName: string;
  password: string;
};
