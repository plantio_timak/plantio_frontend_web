import {
  LABEL_BIG,
  LABEL_BIGGER,
  LABEL_NORMAL,
  LABEL_SMALL,
  LABEL_SMALLER,
} from "../constants/Label";

export type LabelSize =
  | typeof LABEL_SMALLER
  | typeof LABEL_SMALL
  | typeof LABEL_NORMAL
  | typeof LABEL_BIG
  | typeof LABEL_BIGGER;
