export type LineChartData<T> = {
  keys: Array<LineChartDataKey>;
  data: Array<T>;
};

export type LineChartDataKey = {
  label: string;
  value: string;
  defaultVisible: boolean;
};

export type Line = LineChartDataKey & {
  visible: boolean;
};
