import { Service } from "./Services";

export type AddModuleParams = {
  deviceId: string;
  serviceType: Service;
};
