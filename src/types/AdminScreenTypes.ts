import { Device } from "./Device";

export type AdminParams = {
  id: string;
  ip: string;
  name: string;
  devices: Array<Device>;
};
