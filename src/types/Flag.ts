import { IS_ADMIN } from "../constants/Flags";

export type Flag = typeof IS_ADMIN;
