import { NotificationType } from "./Notifications";

export type UseNotificationsState = {
  notify: (text: string, type: NotificationType) => void;
};

export type UseCheckErrorCodeState = {
  checkErrorCode: (errorCode: string) => void;
};

export type UseEventSourceHookState = {
  openEventSourceEmiter: () => void;
  closeEventSourceEmiter: () => void;
};

export type UseWindowDimensionsState = {
  width: number;
  height: number;
};
