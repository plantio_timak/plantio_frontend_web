import { PlantMode } from "./PlantsModule";

export type EditPlantParams = {
  name: string;
  plant: string;
};

export type EditPlantModeParams = {
  plantMode: PlantMode;
  time?: string;
  wateringModeId?: number;
};
