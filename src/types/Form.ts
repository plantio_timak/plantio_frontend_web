export type FormFooterLink = {
  label: string;
  to: string;
};
