import {
  BACK_ICON,
  CHECK_ICON,
  EDIT_ICON,
  GRAPH_ICON,
  ICON_BIG,
  ICON_NORMAL,
  ICON_SMALL,
  LOGOUT_ICON,
  USER_ICON,
} from "../constants/Icon";

export type IconName =
  | typeof USER_ICON
  | typeof LOGOUT_ICON
  | typeof EDIT_ICON
  | typeof BACK_ICON
  | typeof GRAPH_ICON
  | typeof CHECK_ICON;

export type IconSize = typeof ICON_NORMAL | typeof ICON_BIG | typeof ICON_SMALL;
