export type ListPickerItemType = {
  label: string;
  value: string | number;
};
