import { Service } from "./Services";
import { Flag } from "./Flag";

export type User = {
  userName: string | null;
  email: string | null;
  token: string | null;
  services: Array<Service>;
  flags: Array<Flag>;
  selectedService: Service | null;
};
