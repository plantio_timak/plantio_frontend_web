import {
  ERROR_NOTIF,
  SUCCESS_NOTIF,
  WARNING_NOTIF,
} from "../constants/Notifications";

export type NotificationType =
  | typeof SUCCESS_NOTIF
  | typeof ERROR_NOTIF
  | typeof WARNING_NOTIF;
