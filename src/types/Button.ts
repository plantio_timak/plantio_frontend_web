import {
  BUTTON_NORMAL,
  BUTTON_PRIMARY,
  BUTTON_SECONDARY,
  BUTTON_SMALL,
} from "../constants/Button";

export type ButtonSize = typeof BUTTON_NORMAL | typeof BUTTON_SMALL;

export type ButtonType = typeof BUTTON_PRIMARY | typeof BUTTON_SECONDARY;
