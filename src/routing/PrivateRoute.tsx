import React from "react";
import { useSelector } from "react-redux";
import { Redirect, Route, RouteProps } from "react-router-dom";

import { LOGIN_SCREEN_PATH } from "../constants/Screens";
import { getUserSelector } from "../redux/modules/user/reducer";
import { StoreState } from "../types/StoreState";

const PrivateRoute: React.FC<RouteProps> = ({
  component: Component,
  ...rest
}) => {
  const user = useSelector((state: StoreState) => getUserSelector(state.user));

  const getIsUserLogged = (): boolean => !!user.token;

  const isLoggedUser = getIsUserLogged();

  if (isLoggedUser && Component) {
    return (
      <Route {...rest} render={(props) => <Component {...props} />} exact />
    );
  }

  return <Redirect to={LOGIN_SCREEN_PATH} />;
};

export default PrivateRoute;
