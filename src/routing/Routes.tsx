import React from "react";
import { BrowserRouter, Route, Switch } from "react-router-dom";

import LoginScreen from "../screens/LoginScreen";
import RegisterScreen from "../screens/RegisterScreen";
import DashboardScreen from "../screens/DashboardScreen";
import PlantDetailsScreen from "../screens/modules/PlantsModule/PlantDetailsScreen";
import AdminScreenFetcher from "../components/modules/PlantsModule/AdminScreen/AdminScreenFetcher";
import PrivateRoute from "./PrivateRoute";
import {
  ADMIN_SCREEN_PATH,
  DASHBOARD_SCREEN_PATH,
  DEFAULT_SCREEN_PATH,
  LOGIN_SCREEN_PATH,
  PLANT_DETAILS_SCREEN_PATH,
  REGISTER_SCREEN_PATH,
} from "../constants/Screens";

// TODO : porozmyslat ako spravit default path lepsie
const Routes: React.FC = () => (
  <BrowserRouter>
    <Switch>
      <Route
        path={[DEFAULT_SCREEN_PATH, LOGIN_SCREEN_PATH]}
        component={LoginScreen}
        exact
      />
      <Route path={REGISTER_SCREEN_PATH} component={RegisterScreen} exact />
      <PrivateRoute path={DASHBOARD_SCREEN_PATH} component={DashboardScreen} />
      <PrivateRoute path={ADMIN_SCREEN_PATH} component={AdminScreenFetcher} />
      <PrivateRoute
        path={PLANT_DETAILS_SCREEN_PATH}
        component={PlantDetailsScreen}
      />
    </Switch>
  </BrowserRouter>
);

export default Routes;
