import React from "react";
import { PersistGate } from "redux-persist/integration/react";
import { Provider } from "react-redux";
import { ToastContainer } from "react-toastify";

import Routes from "./routing/Routes";
import { persistor, store } from "./redux/store";

import "react-toastify/dist/ReactToastify.css";
import "./styles/notifications.scss";

const App = (): React.ReactElement => (
  <Provider store={store}>
    <ToastContainer position="bottom-right" />
    <PersistGate persistor={persistor}>
      <Routes />
    </PersistGate>
  </Provider>
);

export default App;
