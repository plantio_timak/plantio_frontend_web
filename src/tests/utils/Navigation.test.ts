import { PLANTS_MODULE } from "../../constants/Services";
import { Service } from "../../types/Services";
import {
  getNavigationItemValue,
  getNavigationItems,
  getNavigationModuleLabel,
} from "../../utils/Navigation";

const MOCK_MODULES: Array<Service> = [PLANTS_MODULE];
const MOCK_NAV_ITEMS = ["plants module"];

describe("Navigation utils - getNavigationModuleLabel", () => {
  it("gets module - falsy", () => {
    const result = getNavigationModuleLabel("");

    expect(result).toEqual("");
  });

  it("gets module - PLANTS_MODULE", () => {
    const result = getNavigationModuleLabel(PLANTS_MODULE);

    expect(result).toEqual("plants module");
  });
});

describe("Navigation utils - getNavigationItemValue", () => {
  it("gets navigation item - falsy", () => {
    const result = getNavigationItemValue("");

    expect(result).toEqual(null);
  });

  it("gets navigation item - plants module", () => {
    const result = getNavigationItemValue("plants module");

    expect(result).toEqual(PLANTS_MODULE);
  });
});

describe("Navigation utils - getNavigationItems", () => {
  it("gets falsy modules", () => {
    const result = getNavigationItems((null as unknown) as Array<Service>);

    expect(result).toEqual(undefined);
  });

  it("gets empty modules", () => {
    const result = getNavigationItems([]);

    expect(result).toEqual([]);
  });

  it("gets correct modules", () => {
    const result = getNavigationItems(MOCK_MODULES);

    expect(result).toEqual(MOCK_NAV_ITEMS);
  });
});
