import {
  getUpdatedPlants,
  getWateringModeLabel,
} from "../../utils/PlantsModule";
import { PlantData, PlantMode } from "../../types/PlantsModule";
import {
  PLANT_MODE_INTERACTIVE,
  PLANT_MODE_TIME,
} from "../../constants/PlantsModule";

const UNKNOWN_EDITED_PLANT: PlantData = {
  id: -1,
  name: "plant-1",
  plant: "plant",
  mode: {
    plantMode: PLANT_MODE_TIME,
    time: "8",
  },
  plantValues: [
    {
      time: "time",
      topic: "topic",
      values: {
        humidity: 100,
        temperature: 100,
      },
    },
  ],
};

const EDITED_PLANT: PlantData = {
  id: 0,
  name: "plant0",
  plant: "plant",
  mode: {
    plantMode: PLANT_MODE_TIME,
    time: "8",
  },
  plantValues: [
    {
      time: "time",
      topic: "topic",
      values: {
        humidity: 100,
        temperature: 100,
      },
    },
  ],
};

const MOCK_PLANTS_DEFAULT: Array<PlantData> = [
  {
    id: 0,
    name: "plant0",
    plant: "plant",
    mode: {
      plantMode: PLANT_MODE_INTERACTIVE,
    },
    wateringMode: {
      id: 0,
      name: "mode",
    },
    plantValues: [
      {
        time: "time",
        topic: "topic",
        values: {
          humidity: 100,
          temperature: 100,
        },
      },
    ],
  },
  {
    id: 1,
    name: "plant1",
    plant: "plant",
    mode: {
      plantMode: PLANT_MODE_TIME,
      time: "8",
    },
    plantValues: [
      {
        time: "time",
        topic: "topic",
        values: {
          humidity: 100,
          temperature: 100,
        },
      },
    ],
  },
];

const MOCK_PLANTS_UPDATED: Array<PlantData> = [
  EDITED_PLANT,
  {
    id: 1,
    name: "plant1",
    plant: "plant",
    mode: {
      plantMode: PLANT_MODE_TIME,
      time: "8",
    },
    plantValues: [
      {
        time: "time",
        topic: "topic",
        values: {
          humidity: 100,
          temperature: 100,
        },
      },
    ],
  },
];

describe("PlantsModule utils - getWateringModeLabel", () => {
  it("gets plant mode - falsy", () => {
    const result = getWateringModeLabel(null as unknown as PlantMode);

    expect(result).toEqual("");
  });

  it("gets plant mode - PLANT_MODE_INTERACTIVE", () => {
    const result = getWateringModeLabel(PLANT_MODE_INTERACTIVE);

    expect(result).toEqual("Adaptive");
  });

  it("gets plant mode - PLANT_MODE_TIME", () => {
    const result = getWateringModeLabel(PLANT_MODE_TIME);

    expect(result).toEqual("Time");
  });
});

describe("PlantsModule utils - getUpdatedPlants", () => {
  it("gets empty plants", () => {
    const result = getUpdatedPlants([], null as unknown as PlantData);

    expect(result).toEqual([]);
  });

  it("gets falsy edited plant", () => {
    const result = getUpdatedPlants(
      MOCK_PLANTS_DEFAULT,
      null as unknown as PlantData
    );

    expect(result).toEqual(MOCK_PLANTS_DEFAULT);
  });

  it("plants array does not contain edited plant", () => {
    const result = getUpdatedPlants(MOCK_PLANTS_DEFAULT, UNKNOWN_EDITED_PLANT);

    expect(result).toEqual(MOCK_PLANTS_DEFAULT);
  });

  it("plants array contain edited plant", () => {
    const result = getUpdatedPlants(MOCK_PLANTS_DEFAULT, EDITED_PLANT);

    expect(result).toEqual(MOCK_PLANTS_UPDATED);
  });
});
