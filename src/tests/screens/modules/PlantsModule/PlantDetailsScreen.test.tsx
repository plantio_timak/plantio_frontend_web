import React from "react";
import { render } from "@testing-library/react";

import PlantDetailsScreen from "../../../../screens/modules/PlantsModule/PlantDetailsScreen";
import MockProvider from "../../../MockProvider";

it("renders PlantDetailsScreen correctly", () => {
  const plantDetailsScreen = render(
    <MockProvider>
      <PlantDetailsScreen />
    </MockProvider>
  );

  expect(plantDetailsScreen).toMatchSnapshot();
});
