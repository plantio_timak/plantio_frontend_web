import React from "react";
import { render } from "@testing-library/react";

import RegisterScreen from "../../screens/RegisterScreen";
import MockProvider from "../MockProvider";

it("renders RegisterScreen correctly", () => {
  const registerScreen = render(
    <MockProvider>
      <RegisterScreen />
    </MockProvider>
  );

  expect(registerScreen).toMatchSnapshot();
});
