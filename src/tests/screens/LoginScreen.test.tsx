import React from "react";
import { render } from "@testing-library/react";

import LoginScreen from "../../screens/LoginScreen";
import MockProvider from "../MockProvider";

it("renders LoginScreen correctly", () => {
  const loginScreen = render(
    <MockProvider>
      <LoginScreen />
    </MockProvider>
  );

  expect(loginScreen).toMatchSnapshot();
});
