import React from "react";
import { render } from "@testing-library/react";

import DashboardScreen from "../../screens/DashboardScreen";
import MockProvider from "../MockProvider";

it("renders DashboardScreen correctly", () => {
  const dashboardScreen = render(
    <MockProvider>
      <DashboardScreen />
    </MockProvider>
  );

  expect(dashboardScreen).toMatchSnapshot();
});
