import React from "react";
import { Provider } from "react-redux";
import { combineReducers, createStore } from "redux";
import { Router } from "react-router-dom";
import { createMemoryHistory } from "history";

import user from "../redux/modules/user/reducer";
import plantsModule from "../redux/modules/plantsModule/reducer";

const MockProvider: React.FC = ({ children }) => {
  const history = createMemoryHistory();
  const store = createStore(
    combineReducers({
      user,
      plantsModule,
    })
  );

  return (
    <Provider store={store}>
      <Router history={history}>{children}</Router>
    </Provider>
  );
};

export default MockProvider;
