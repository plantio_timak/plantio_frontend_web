import React from "react";

import Screen from "../components/templates/screen/Screen";
import DashboardScreenContent from "../components/organisms/DashboardScreen/DashboardScreenContent";

const DashboardScreen: React.FC = () => (
  <Screen>
    <DashboardScreenContent />
  </Screen>
);

export default DashboardScreen;
