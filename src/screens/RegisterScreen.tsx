import React from "react";

import Screen from "../components/templates/screen/Screen";
import RegisterScreenContent from "../components/organisms/RegisterScreen/RegisterScreenContent";

const RegisterScreen: React.FC = () => (
  <Screen showNavigation={false} showUser={false}>
    <RegisterScreenContent />
  </Screen>
);

export default RegisterScreen;
