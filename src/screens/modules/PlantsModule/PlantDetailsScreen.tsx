import React from "react";

import PlantDetailsScreenFetcher from "../../../components/modules/PlantsModule/PlantDetailsScreen/PlantDetailsScreenFetcher";
import Screen from "../../../components/templates/screen/Screen";

const PlantDetailsScreen: React.FC = () => (
  <Screen>
    <PlantDetailsScreenFetcher />
  </Screen>
);

export default PlantDetailsScreen;
