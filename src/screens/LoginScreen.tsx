import React from "react";

import Screen from "../components/templates/screen/Screen";
import LoginScreenContent from "../components/organisms/LoginScreen/LoginScreenContent";

const LoginScreen: React.FC = () => (
  <Screen showNavigation={false} showUser={false}>
    <LoginScreenContent />
  </Screen>
);

export default LoginScreen;
