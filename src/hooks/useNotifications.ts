import { ToastOptions, toast } from "react-toastify";

import { UseNotificationsState } from "../types/HooksStates";
import { NotificationType } from "../types/Notifications";

const useNotifications = (): UseNotificationsState => {
  const notify = (text: string, type: NotificationType): void => {
    const options: ToastOptions = { type };

    toast(text, options);
  };

  return {
    notify,
  };
};

export default useNotifications;
