import { useDispatch } from "react-redux";

import useNotifications from "./useNotifications";
import useEventSourceHook from "./useEventSourceHook";
import { TOKEN_INVALID_ERROR_CODE } from "../constants/ErrorCodes";
import { WARNING_NOTIF } from "../constants/Notifications";
import { userLogoutActionCreator } from "../redux/modules/user/actions";
import { UseCheckErrorCodeState } from "../types/HooksStates";

const useCheckErrorCode = (): UseCheckErrorCodeState => {
  const dispatch = useDispatch();
  const { notify } = useNotifications();
  const { closeEventSourceEmiter } = useEventSourceHook();

  const checkErrorCode = (errorCode: string): void => {
    if (errorCode === TOKEN_INVALID_ERROR_CODE) {
      notify("Session expired. You will be logged out", WARNING_NOTIF);
      setTimeout(() => {
        closeEventSourceEmiter();
        dispatch(userLogoutActionCreator());
      }, 5000);
    }
  };

  return {
    checkErrorCode,
  };
};

export default useCheckErrorCode;
