import { useEffect, useState } from "react";
import { useDispatch } from "react-redux";

import { eventSourceApi } from "../api/api";
import {
  REAL_TIME_PLANT_TOPIC,
  REAL_TIME_ROOM_TOPIC,
} from "../constants/PlantsModule";
import {
  plantsModuleUpdatePlantValuesActionCreator,
  plantsModuleUpdateRoomValuesActionCreator,
} from "../redux/modules/plantsModule/actions";
import { UseEventSourceHookState } from "../types/HooksStates";
import {
  PlantDataApi,
  PlantsModulePlantRealTimeFields,
  PlantsModuleRealTimeApi,
  PlantsModuleRoomRealTimeFields,
  RoomDataApi,
} from "../types/PlantsModule";

const useEventSourceHook = (): UseEventSourceHookState => {
  const dispatch = useDispatch();
  const [eventSource, setEventSource] = useState<EventSource>();

  useEffect(() => {
    if (eventSource) {
      eventSource.onmessage = onMessageHandler;
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [eventSource]);

  const openEventSourceEmiter = () => {
    closeEventSourceEmiter();

    const api = eventSourceApi();

    setEventSource(api);
  };

  const closeEventSourceEmiter = () => {
    if (eventSource) {
      eventSource.close();
    }
  };

  const onMessageHandler = (event: MessageEvent) => {
    if (event?.data) {
      const data: PlantsModuleRealTimeApi = JSON.parse(event.data);
      // until hardware will send correct date
      const mockDate = new Date();

      switch (data.measurement) {
        case REAL_TIME_ROOM_TOPIC: {
          const fields = data.fields as PlantsModuleRoomRealTimeFields;

          const values: RoomDataApi = {
            time: mockDate.toISOString(),
            topic: data.measurement,
            values: {
              humidity: parseInt(fields.humidity),
              lightIntensity: parseInt(fields.light_intensity),
              temperature: parseInt(fields.temperature),
              waterCapacity: false,
            },
          };

          dispatch(plantsModuleUpdateRoomValuesActionCreator(values));

          break;
        }
        case REAL_TIME_PLANT_TOPIC: {
          const fields = data.fields as PlantsModulePlantRealTimeFields;

          const values: PlantDataApi = {
            time: mockDate.toISOString(),
            topic: data.measurement,
            values: {
              humidity: parseInt(fields.humidity),
              temperature: parseInt(fields.temperature),
            },
          };

          dispatch(
            plantsModuleUpdatePlantValuesActionCreator(
              values,
              parseInt(fields.plant_id)
            )
          );

          break;
        }
        default:
          break;
      }
    }
  };

  return {
    openEventSourceEmiter,
    closeEventSourceEmiter,
  };
};

export default useEventSourceHook;
