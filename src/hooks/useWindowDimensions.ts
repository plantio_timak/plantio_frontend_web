import { useEffect, useState } from "react";

import { UseWindowDimensionsState } from "../types/HooksStates";

const useWindowDimensions = (): UseWindowDimensionsState => {
  const [windowDimensions, setWindowDimensions] =
    useState<UseWindowDimensionsState>({
      width: 0,
      height: 0,
    });

  const handleResize = () => {
    const width =
      window.innerWidth ||
      document.documentElement.clientWidth ||
      document.body.clientWidth;

    const height =
      window.innerHeight ||
      document.documentElement.clientHeight ||
      document.body.clientHeight;

    setWindowDimensions({ width, height });
  };

  useEffect(() => {
    window.addEventListener("resize", handleResize);

    handleResize();

    return () => window.removeEventListener("resize", handleResize);
  }, []);

  return windowDimensions;
};

export default useWindowDimensions;
