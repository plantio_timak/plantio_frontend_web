import LocalForage from "localforage";
import createSagaMiddleware from "redux-saga";
import { AnyAction, applyMiddleware, compose, createStore } from "redux";
import { persistReducer, persistStore } from "redux-persist";
import { Reducer } from "react";

import rootReducer from "./reducer";
import rootSaga from "./sagas";
import { fetchStaticDataMiddleware } from "./middleware";

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION_COMPOSE__?: typeof compose;
  }
}

const persistConfig = {
  key: "root",
  storage: LocalForage,
  whitelist: ["user", "plantsModule"],
};

const sagaMiddleware = createSagaMiddleware();

const persistedReducer = persistReducer(
  persistConfig,
  rootReducer as Reducer<unknown, AnyAction>
);

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const enhancer = composeEnhancers(
  applyMiddleware(sagaMiddleware, fetchStaticDataMiddleware)
);

const store = createStore(persistedReducer, enhancer);

const persistor = persistStore(store);

sagaMiddleware.run(rootSaga);

export { store, persistor };
