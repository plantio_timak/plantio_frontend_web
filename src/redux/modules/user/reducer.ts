import {
  USER_ADD_SERVICE_ACTION,
  USER_LOGIN_ACTION,
  USER_LOGIN_ACTION_FAILURE,
  USER_LOGIN_ACTION_SUCCESS,
  USER_LOGOUT_ACTION,
  USER_REGISTER_ACTION,
  USER_REGISTER_ACTION_FAILURE,
  USER_REGISTER_ACTION_SUCCESS,
  USER_SET_SELECTED_SERVICE_ACTION,
  UserAction,
} from "./actions";
import { User } from "../../../types/User";
import { Service } from "../../../types/Services";

export type UserState = {
  user: User;
  isUserFetching: boolean;
  userFetchError: string | null;
};

export const initialState = {
  user: {
    userName: null,
    email: null,
    token: null,
    flags: [],
    services: [],
    selectedService: null,
  },
  isUserFetching: false,
  userFetchError: null,
};

const userReducer = (
  state: UserState = initialState,
  action: UserAction
): UserState => {
  switch (action.type) {
    case USER_REGISTER_ACTION:
    case USER_LOGIN_ACTION: {
      return {
        ...state,
        isUserFetching: true,
      };
    }
    case USER_REGISTER_ACTION_SUCCESS:
    case USER_LOGIN_ACTION_SUCCESS: {
      return {
        ...state,
        isUserFetching: false,
        userFetchError: null,
        user: action.payload.user,
      };
    }
    case USER_REGISTER_ACTION_FAILURE:
    case USER_LOGIN_ACTION_FAILURE: {
      return {
        ...state,
        isUserFetching: false,
        userFetchError: action.payload.error,
      };
    }
    case USER_SET_SELECTED_SERVICE_ACTION: {
      return {
        ...state,
        user: {
          ...state.user,
          selectedService: action.payload.service,
        },
      };
    }
    case USER_ADD_SERVICE_ACTION: {
      const updatedServices = state.user.services
        ? [...state.user.services, action.payload.service]
        : [action.payload.service];

      return {
        ...state,
        user: {
          ...state.user,
          services: updatedServices,
        },
      };
    }
    case USER_LOGOUT_ACTION: {
      return initialState;
    }
    default: {
      return state;
    }
  }
};

export const getUserIsFetchingSelector = (state: UserState): boolean =>
  state?.isUserFetching;
export const getUserFetchErrorSelector = (state: UserState): string | null =>
  state?.userFetchError;
export const getUserSelector = (state: UserState): User => state?.user;
export const getUserTokenSelector = (state: UserState): string | null =>
  state?.user?.token;
export const getUserServicesSelector = (state: UserState): Array<Service> =>
  state?.user?.services;
export const getUserSelectedServiceSelector = (
  state: UserState
): Service | null => state?.user?.selectedService;

export default userReducer;
