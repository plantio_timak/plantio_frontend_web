import { call, put, takeEvery } from "redux-saga/effects";
import { AxiosResponse } from "axios";

import {
  USER_LOGIN_ACTION,
  USER_REGISTER_ACTION,
  UserLoginAction,
  UserRegisterAction,
  userLoginFailureActionCreator,
  userLoginSuccessActionCreator,
  userRegisterFailureActionCreator,
  userRegisterSuccessActionCreator,
} from "./actions";
import { api } from "../../../api/api";
import {
  getLoginUserApiPath,
  getRegisterUserApiPath,
} from "../../../api/apiPaths";
import { ERROR_NOTIF, SUCCESS_NOTIF } from "../../../constants/Notifications";
import { FIELD_INVALID_ERROR_CODE } from "../../../constants/ErrorCodes";
import { PLANTS_MODULE } from "../../../constants/Services";
import { plantsModuleSetSelectedRoomSuccessActionCreator } from "../plantsModule/actions";
import { IS_ADMIN } from "../../../constants/Flags";
import {
  ADMIN_SCREEN_PATH,
  DASHBOARD_SCREEN_PATH,
} from "../../../constants/Screens";

function* userRegister(action: UserRegisterAction) {
  const { payload } = action;
  const { params, redirectToDashboard, notify } = payload;

  try {
    const response: AxiosResponse = yield call(
      api().post,
      getRegisterUserApiPath(),
      params
    );

    yield put(userRegisterSuccessActionCreator(response.data));

    notify("User successfully registered and logged in", SUCCESS_NOTIF);
    redirectToDashboard();
  } catch (error) {
    const [errorObject = null] = error?.response?.data?.errors || [];

    const message =
      errorObject?.error === FIELD_INVALID_ERROR_CODE
        ? "User with selected username already exists"
        : "Error occurred during registration";

    yield put(userRegisterFailureActionCreator(message));

    notify(message, ERROR_NOTIF);
  }
}

function* userLogin(action: UserLoginAction) {
  const { payload } = action;
  const { params, redirectToDashboard, notify } = payload;

  try {
    const response: AxiosResponse = yield call(
      api().post,
      getLoginUserApiPath(),
      params
    );

    const selectedService = response.data?.lastUsedService;

    const formatedData = {
      ...response.data,
      selectedService,
    };

    yield put(userLoginSuccessActionCreator(formatedData));

    if (selectedService === PLANTS_MODULE) {
      yield put(
        plantsModuleSetSelectedRoomSuccessActionCreator(
          response.data?.lastUsedServiceInstance
        )
      );
    }

    notify("User successfully logged in", SUCCESS_NOTIF);
    if (response.data.flags?.includes(IS_ADMIN)) {
      redirectToDashboard(ADMIN_SCREEN_PATH);
    } else redirectToDashboard(DASHBOARD_SCREEN_PATH);
  } catch (error) {
    const [errorObject = null] = error?.response?.data?.errors || [];

    const message =
      errorObject?.error === FIELD_INVALID_ERROR_CODE
        ? "Wrong username or password"
        : "Error occurred during logging in";

    yield put(userLoginFailureActionCreator(message));

    notify(message, ERROR_NOTIF);
  }
}

function* userRegisterSaga() {
  yield takeEvery(USER_REGISTER_ACTION, userRegister);
}

function* userLoginSaga() {
  yield takeEvery(USER_LOGIN_ACTION, userLogin);
}

export default [userRegisterSaga(), userLoginSaga()];
