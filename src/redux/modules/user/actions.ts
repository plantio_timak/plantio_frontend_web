import { LoginParams } from "../../../types/LoginScreenTypes";
import { NotificationType } from "../../../types/Notifications";
import { RegisterParams } from "../../../types/RegisterScreenTypes";
import { Service } from "../../../types/Services";
import { User } from "../../../types/User";

export const USER_REGISTER_ACTION = "USER_REGISTER_ACTION";
export const USER_REGISTER_ACTION_SUCCESS = "USER_REGISTER_ACTION_SUCCESS";
export const USER_REGISTER_ACTION_FAILURE = "USER_REGISTER_ACTION_FAILURE";
export const USER_LOGIN_ACTION = "USER_LOGIN_ACTION";
export const USER_LOGIN_ACTION_SUCCESS = "USER_LOGIN_ACTION_SUCCESS";
export const USER_LOGIN_ACTION_FAILURE = "USER_LOGIN_ACTION_FAILURE";
export const USER_LOGOUT_ACTION = "USER_LOGOUT_ACTION";
export const USER_SET_SELECTED_SERVICE_ACTION =
  "USER_SET_SELECTED_SERVICE_ACTION";
export const USER_ADD_SERVICE_ACTION = "USER_ADD_SERVICE_ACTION";

export type UserRegisterAction = {
  type: typeof USER_REGISTER_ACTION;
  payload: {
    params: RegisterParams;
    notify: (text: string, type: NotificationType) => void;
    redirectToDashboard: () => void;
  };
};
export type UserRegisterSuccessAction = {
  type: typeof USER_REGISTER_ACTION_SUCCESS;
  payload: {
    user: User;
  };
};
export type UserRegisterFailureAction = {
  type: typeof USER_REGISTER_ACTION_FAILURE;
  payload: {
    error: string;
  };
};
export type UserLoginAction = {
  type: typeof USER_LOGIN_ACTION;
  payload: {
    params: LoginParams;
    notify: (text: string, type: NotificationType) => void;
    redirectToDashboard: (path: string) => void;
  };
};
export type UserLoginSuccessAction = {
  type: typeof USER_LOGIN_ACTION_SUCCESS;
  payload: {
    user: User;
  };
};
export type UserLoginFailureAction = {
  type: typeof USER_LOGIN_ACTION_FAILURE;
  payload: {
    error: string;
  };
};
export type UserLogoutAction = {
  type: typeof USER_LOGOUT_ACTION;
};
export type UserSetSelectedServiceAction = {
  type: typeof USER_SET_SELECTED_SERVICE_ACTION;
  payload: {
    service: Service;
  };
};
export type UserAddServiceAction = {
  type: typeof USER_ADD_SERVICE_ACTION;
  payload: {
    service: Service;
  };
};

export type UserAction =
  | UserRegisterAction
  | UserRegisterSuccessAction
  | UserRegisterFailureAction
  | UserLoginAction
  | UserLoginSuccessAction
  | UserLoginFailureAction
  | UserLogoutAction
  | UserSetSelectedServiceAction
  | UserAddServiceAction;

export const userRegisterActionCreator = (
  params: RegisterParams,
  notify: (text: string, type: NotificationType) => void,
  redirectToDashboard: () => void
): UserAction => ({
  type: USER_REGISTER_ACTION,
  payload: { params, notify, redirectToDashboard },
});
export const userRegisterSuccessActionCreator = (user: User): UserAction => ({
  type: USER_REGISTER_ACTION_SUCCESS,
  payload: { user },
});
export const userRegisterFailureActionCreator = (
  error: string
): UserAction => ({
  type: USER_REGISTER_ACTION_FAILURE,
  payload: { error },
});
export const userLoginActionCreator = (
  params: LoginParams,
  notify: (text: string, type: NotificationType) => void,
  redirectToDashboard: (path: string) => void
): UserAction => ({
  type: USER_LOGIN_ACTION,
  payload: { params, notify, redirectToDashboard },
});
export const userLoginSuccessActionCreator = (user: User): UserAction => ({
  type: USER_LOGIN_ACTION_SUCCESS,
  payload: { user },
});
export const userLoginFailureActionCreator = (error: string): UserAction => ({
  type: USER_LOGIN_ACTION_FAILURE,
  payload: { error },
});
export const userLogoutActionCreator = (): UserAction => ({
  type: USER_LOGOUT_ACTION,
});
export const userSetSelectedServiceActionCreator = (
  service: Service
): UserAction => ({
  type: USER_SET_SELECTED_SERVICE_ACTION,
  payload: { service },
});
export const userAddServiceActionCreator = (service: Service): UserAction => ({
  type: USER_ADD_SERVICE_ACTION,
  payload: { service },
});
