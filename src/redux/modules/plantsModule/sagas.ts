import { AxiosResponse } from "axios";
import {
  AllEffect,
  CallEffect,
  PutEffect,
  all,
  call,
  put,
  takeEvery,
} from "redux-saga/effects";

import { api } from "../../../api/api";
import {
  getRoomDataApiPath,
  getRoomPlantsDataApiPath,
  getRoomsApiPath,
  getSetLastUsedInstanceApiPath,
  getWateringModesApiPath,
} from "../../../api/apiPaths";
import { ERROR_NOTIF } from "../../../constants/Notifications";
import {
  PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
  PLANTS_MODULE_FETCH_WATERING_MODES_ACTION,
  PLANTS_MODULE_SET_SELECTED_ROOM_ACTION,
  PlantsModuleAction,
  PlantsModuleFetchRoomsListAction,
  PlantsModuleFetchSelectedRoomDataAction,
  PlantsModuleFetchWateringModesAction,
  PlantsModuleSetSelectedRoomAction,
  plantsModuleFetchRoomsListFailureActionCreator,
  plantsModuleFetchRoomsListSuccessActionCreator,
  plantsModuleFetchSelectedRoomDataFailureActionCreator,
  plantsModuleFetchSelectedRoomDataSuccessActionCreator,
  plantsModuleFetchWateringModesFailureActionCreator,
  plantsModuleFetchWateringModesSuccessActionCreator,
  plantsModuleSetSelectedRoomSuccessActionCreator,
} from "./actions";

function* fetchRoomsList(action: PlantsModuleFetchRoomsListAction) {
  const { token, notify } = action.payload;

  try {
    const response: AxiosResponse = yield call(
      api(token).get,
      getRoomsApiPath()
    );

    yield put(
      plantsModuleFetchRoomsListSuccessActionCreator(response.data?.userRooms)
    );
  } catch (error) {
    const message = "Failed to fetch user rooms";

    yield put(plantsModuleFetchRoomsListFailureActionCreator(message));

    notify(message, ERROR_NOTIF);
  }
}

function* fetchSelectedRoomData(
  action: PlantsModuleFetchSelectedRoomDataAction
): Generator<
  AllEffect<CallEffect<unknown>> | PutEffect<PlantsModuleAction>,
  void,
  [AxiosResponse, AxiosResponse]
> {
  const { token, notify, roomId, dataDuration } = action.payload;

  try {
    const [roomDataResponse, roomPlantsDataResponse]: Array<AxiosResponse> =
      yield all([
        call(api(token).get, getRoomDataApiPath(roomId, dataDuration)),
        call(api(token).get, getRoomPlantsDataApiPath(roomId, dataDuration)),
      ]);

    yield put(
      plantsModuleFetchSelectedRoomDataSuccessActionCreator({
        data: roomDataResponse?.data,
        ...roomPlantsDataResponse?.data,
      })
    );
  } catch (error) {
    const message = "Failed to fetch selected room data";

    yield put(plantsModuleFetchSelectedRoomDataFailureActionCreator(message));
    notify(message, ERROR_NOTIF);
  }
}

function* setSelectedRoom(action: PlantsModuleSetSelectedRoomAction) {
  const { token, roomId, notify } = action.payload;
  try {
    yield call(api(token).put, getSetLastUsedInstanceApiPath(), {
      lastUsedServiceInstanceId: roomId,
    });

    yield put(plantsModuleSetSelectedRoomSuccessActionCreator(roomId));
  } catch (error) {
    yield put(plantsModuleSetSelectedRoomSuccessActionCreator(roomId));

    notify(
      "Error occured during saving selected room on server. Selection won't be remembered for next login",
      ERROR_NOTIF
    );
  }
}

function* fetchWateringModes(action: PlantsModuleFetchWateringModesAction) {
  const { token } = action.payload;
  try {
    const response: AxiosResponse = yield call(
      api(token).get,
      getWateringModesApiPath()
    );

    yield put(
      plantsModuleFetchWateringModesSuccessActionCreator(
        response?.data?.wateringModes
      )
    );
  } catch (error) {
    const message = "Failed to fetch watering modes";

    yield put(plantsModuleFetchWateringModesFailureActionCreator(message));
  }
}

function* fetchRoomsListSaga() {
  yield takeEvery(PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION, fetchRoomsList);
}

function* fetchSelectedRoomDataSaga() {
  yield takeEvery(
    PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
    fetchSelectedRoomData
  );
}

function* setSelectedRoomSaga() {
  yield takeEvery(PLANTS_MODULE_SET_SELECTED_ROOM_ACTION, setSelectedRoom);
}

function* fetchWateringModesSaga() {
  yield takeEvery(
    PLANTS_MODULE_FETCH_WATERING_MODES_ACTION,
    fetchWateringModes
  );
}

export default [
  fetchRoomsListSaga(),
  fetchSelectedRoomDataSaga(),
  setSelectedRoomSaga(),
  fetchWateringModesSaga(),
];
