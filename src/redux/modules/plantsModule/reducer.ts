import { PLANTS_MODULE_DURATION_DAY } from "../../../constants/PlantsModule";
import {
  PlantData,
  PlantsModuleDataDuration,
  Room,
  RoomData,
  WateringMode,
} from "../../../types/PlantsModule";
import {
  getUpdatedPlants,
  updatePlantValues,
  updateRoomValues,
} from "../../../utils/PlantsModule";
import {
  PLANTS_MODULE_EDIT_PLANT_DATA_ACTION,
  PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION,
  PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION,
  PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION,
  PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION,
  PLANTS_MODULE_FETCH_WATERING_MODES_ACTION,
  PLANTS_MODULE_FETCH_WATERING_MODES_FAILURE_ACTION,
  PLANTS_MODULE_FETCH_WATERING_MODES_SUCCESS_ACTION,
  PLANTS_MODULE_SET_DATA_DURATION_ACTION,
  PLANTS_MODULE_SET_SELECTED_ROOM_ACTION,
  PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION,
  PLANTS_MODULE_UPDATE_PLANT_VALUES_ACTION,
  PLANTS_MODULE_UPDATE_ROOM_VALUES_ACTION,
  PlantsModuleAction,
} from "./actions";

export type PlantsModuleState = {
  isRoomsFetching: boolean;
  isSelectedRoomDataFetching: boolean;
  isSelectedRoomSaveFetching: boolean;
  isWateringModesFetching: boolean;
  roomsFetchError: string | null;
  selectedRoomDataFetchError: string | null;
  wateringModesFetchError: string | null;
  rooms: Array<Room>;
  selectedRoom: number | null;
  selectedRoomData: RoomData | null;
  wateringModes: Array<WateringMode>;
  dataDuration: PlantsModuleDataDuration;
};

export const initialState: PlantsModuleState = {
  isRoomsFetching: false,
  isSelectedRoomDataFetching: false,
  isSelectedRoomSaveFetching: false,
  isWateringModesFetching: false,
  roomsFetchError: null,
  selectedRoomDataFetchError: null,
  wateringModesFetchError: null,
  rooms: [],
  selectedRoom: null,
  selectedRoomData: null,
  wateringModes: [],
  dataDuration: PLANTS_MODULE_DURATION_DAY,
};

const plantsModuleReducer = (
  state: PlantsModuleState = initialState,
  action: PlantsModuleAction
): PlantsModuleState => {
  switch (action.type) {
    case PLANTS_MODULE_SET_SELECTED_ROOM_ACTION: {
      return { ...state, isSelectedRoomSaveFetching: true };
    }
    case PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION: {
      return {
        ...state,
        isSelectedRoomSaveFetching: false,
        selectedRoom: action.payload.roomId,
      };
    }
    case PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION: {
      return { ...state, isRoomsFetching: true };
    }
    case PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION: {
      return { ...state, isRoomsFetching: false, rooms: action.payload.rooms };
    }
    case PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION: {
      return {
        ...state,
        isRoomsFetching: false,
        roomsFetchError: action.payload.error,
      };
    }
    case PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION: {
      return {
        ...state,
        isSelectedRoomDataFetching: true,
      };
    }
    case PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION: {
      return {
        ...state,
        isSelectedRoomDataFetching: false,
        selectedRoomData: action.payload.roomData,
      };
    }
    case PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION: {
      return {
        ...state,
        isSelectedRoomDataFetching: false,
        selectedRoomDataFetchError: action.payload.error,
      };
    }
    case PLANTS_MODULE_EDIT_PLANT_DATA_ACTION: {
      return {
        ...state,
        selectedRoomData: {
          ...(state.selectedRoomData as RoomData),
          plants: getUpdatedPlants(
            state.selectedRoomData?.plants || [],
            action.payload.data
          ),
        },
      };
    }
    case PLANTS_MODULE_FETCH_WATERING_MODES_ACTION: {
      return {
        ...state,
        isWateringModesFetching: true,
      };
    }
    case PLANTS_MODULE_FETCH_WATERING_MODES_SUCCESS_ACTION: {
      return {
        ...state,
        isWateringModesFetching: false,
        wateringModes: action.payload.data,
      };
    }
    case PLANTS_MODULE_FETCH_WATERING_MODES_FAILURE_ACTION: {
      return {
        ...state,
        isWateringModesFetching: false,
        wateringModesFetchError: action.payload.error,
      };
    }
    case PLANTS_MODULE_SET_DATA_DURATION_ACTION: {
      return {
        ...state,
        dataDuration: action.payload.dataDuration,
      };
    }
    case PLANTS_MODULE_UPDATE_ROOM_VALUES_ACTION: {
      return {
        ...state,
        selectedRoomData: {
          ...(state.selectedRoomData as RoomData),
          data: updateRoomValues(
            state.selectedRoomData?.data,
            action.payload.data
          ),
        },
      };
    }
    case PLANTS_MODULE_UPDATE_PLANT_VALUES_ACTION: {
      return {
        ...state,
        selectedRoomData: {
          ...(state.selectedRoomData as RoomData),
          plants: updatePlantValues(
            state.selectedRoomData?.plants,
            action.payload.data,
            action.payload.plantId
          ),
        },
      };
    }
    default: {
      return state;
    }
  }
};

export const getPlantsModuleSelectedRoomSelector = (
  state: PlantsModuleState
): Room | null =>
  state?.rooms?.find((room) => room.id === state?.selectedRoom) || null;
export const getPlantsModuleSelectedRoomDataSelector = (
  state: PlantsModuleState
): RoomData | null => state?.selectedRoomData;
export const getPlantsModuleSelectedRoomPlantsDataSelector = (
  state: PlantsModuleState
): Array<PlantData> => state?.selectedRoomData?.plants || [];
export const getPlantsModuleRoomsListSelector = (
  state: PlantsModuleState
): Array<Room> => state?.rooms;
export const getPlantsModuleIsRoomListFetchingSelector = (
  state: PlantsModuleState
): boolean => state.isRoomsFetching;
export const getPlantsModuleIsSelectedRoomFetchingSelector = (
  state: PlantsModuleState
): boolean => state.isSelectedRoomDataFetching;
export const getPlantsModuleSinglePlantDataSelector = (
  state: PlantsModuleState,
  id: number
): PlantData | null =>
  state?.selectedRoomData?.plants?.find((plant) => plant.id === id) || null;
export const getPlantsModulePlantsDataSelector = (
  state: PlantsModuleState
): Array<PlantData> => state?.selectedRoomData?.plants || [];
export const getPlantsModuleWateringModesSelector = (
  state: PlantsModuleState
): Array<WateringMode> => state?.wateringModes;
export const getPlantsModuleDataDurationSelector = (
  state: PlantsModuleState
): PlantsModuleDataDuration => state?.dataDuration;

export default plantsModuleReducer;
