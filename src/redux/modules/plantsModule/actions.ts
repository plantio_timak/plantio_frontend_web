import { NotificationType } from "../../../types/Notifications";
import {
  PlantData,
  PlantDataApi,
  PlantsModuleDataDuration,
  Room,
  RoomData,
  RoomDataApi,
  WateringMode,
} from "../../../types/PlantsModule";

export const PLANTS_MODULE_SET_SELECTED_ROOM_ACTION =
  "PLANTS_MODULE_SET_SELECTED_ROOM_ACTION";
export const PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION =
  "PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION =
  "PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION";
export const PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION =
  "PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION =
  "PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION";
export const PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION =
  "PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION";
export const PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION =
  "PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION =
  "PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION";
export const PLANTS_MODULE_EDIT_PLANT_DATA_ACTION =
  "PLANT_MODULE_EDIT_PLANT_DATA_ACTION";
export const PLANTS_MODULE_FETCH_WATERING_MODES_ACTION =
  "PLANT_MODULE_FETCH_WATERING_MODES_ACTION";
export const PLANTS_MODULE_FETCH_WATERING_MODES_SUCCESS_ACTION =
  "PLANT_MODULE_FETCH_WATERING_MODES_SUCCESS_ACTION";
export const PLANTS_MODULE_FETCH_WATERING_MODES_FAILURE_ACTION =
  "PLANT_MODULE_FETCH_WATERING_MODES_FAILURE_ACTION";
export const PLANTS_MODULE_SET_DATA_DURATION_ACTION =
  "PLANTS_MODULE_SET_DATA_DURATION_ACTION";
export const PLANTS_MODULE_UPDATE_ROOM_VALUES_ACTION =
  "PLANTS_MODULE_UPDATE_ROOM_VALUES_ACTION";
export const PLANTS_MODULE_UPDATE_PLANT_VALUES_ACTION =
  "PLANTS_MODULE_UPDATE_PLANT_VALUES_ACTION";

export type PlantsModuleSetSelectedRoomAction = {
  type: typeof PLANTS_MODULE_SET_SELECTED_ROOM_ACTION;
  payload: {
    token: string | null;
    roomId: number | null;
    notify: (text: string, type: NotificationType) => void;
  };
};
export type PlantsModuleSetSelectedRoomSuccessAction = {
  type: typeof PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION;
  payload: {
    roomId: number | null;
  };
};

export type PlantsModuleFetchRoomsListAction = {
  type: typeof PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION;
  payload: {
    token: string | null;
    notify: (text: string, type: NotificationType) => void;
  };
};

export type PlantsModuleFetchRoomsListSuccessAction = {
  type: typeof PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION;
  payload: {
    rooms: Array<Room>;
  };
};

export type PlantsModuleFetchRoomsListFailureAction = {
  type: typeof PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION;
  payload: {
    error: string;
  };
};

export type PlantsModuleFetchSelectedRoomDataAction = {
  type: typeof PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION;
  payload: {
    roomId: number;
    token: string | null;
    dataDuration: PlantsModuleDataDuration;
    notify: (text: string, type: NotificationType) => void;
  };
};

export type PlantsModuleFetchSelectedRoomDataSuccessAction = {
  type: typeof PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION;
  payload: {
    roomData: RoomData | null;
  };
};

export type PlantsModuleFetchSelectedRoomDataFailureAction = {
  type: typeof PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION;
  payload: {
    error: string;
  };
};

export type PlantsModuleEditPlantDataAction = {
  type: typeof PLANTS_MODULE_EDIT_PLANT_DATA_ACTION;
  payload: {
    data: PlantData;
  };
};

export type PlantsModuleFetchWateringModesAction = {
  type: typeof PLANTS_MODULE_FETCH_WATERING_MODES_ACTION;
  payload: {
    token: string | null;
  };
};

export type PlantsModuleFetchWateringModesSuccessAction = {
  type: typeof PLANTS_MODULE_FETCH_WATERING_MODES_SUCCESS_ACTION;
  payload: {
    data: Array<WateringMode>;
  };
};

export type PlantsModuleFetchWateringModesFailureAction = {
  type: typeof PLANTS_MODULE_FETCH_WATERING_MODES_FAILURE_ACTION;
  payload: {
    error: string;
  };
};

export type PlantsModuleSetDataDurationAction = {
  type: typeof PLANTS_MODULE_SET_DATA_DURATION_ACTION;
  payload: {
    dataDuration: PlantsModuleDataDuration;
  };
};

export type PlantsModuleUpdateRoomValuesAction = {
  type: typeof PLANTS_MODULE_UPDATE_ROOM_VALUES_ACTION;
  payload: {
    data: RoomDataApi;
  };
};

export type PlantsModuleUpdatePlantValuesAction = {
  type: typeof PLANTS_MODULE_UPDATE_PLANT_VALUES_ACTION;
  payload: {
    data: PlantDataApi;
    plantId: number;
  };
};

export type PlantsModuleAction =
  | PlantsModuleSetSelectedRoomAction
  | PlantsModuleSetSelectedRoomSuccessAction
  | PlantsModuleFetchRoomsListAction
  | PlantsModuleFetchRoomsListSuccessAction
  | PlantsModuleFetchRoomsListFailureAction
  | PlantsModuleFetchSelectedRoomDataAction
  | PlantsModuleFetchSelectedRoomDataSuccessAction
  | PlantsModuleFetchSelectedRoomDataFailureAction
  | PlantsModuleEditPlantDataAction
  | PlantsModuleFetchWateringModesAction
  | PlantsModuleFetchWateringModesSuccessAction
  | PlantsModuleFetchWateringModesFailureAction
  | PlantsModuleSetDataDurationAction
  | PlantsModuleUpdateRoomValuesAction
  | PlantsModuleUpdatePlantValuesAction;

export const plantsModuleSetSelectedRoomActionCreator = (
  roomId: number | null,
  token: string | null,
  notify: (text: string, type: NotificationType) => void
): PlantsModuleAction => ({
  type: PLANTS_MODULE_SET_SELECTED_ROOM_ACTION,
  payload: { roomId, token, notify },
});
export const plantsModuleSetSelectedRoomSuccessActionCreator = (
  roomId: number | null
): PlantsModuleAction => ({
  type: PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION,
  payload: { roomId },
});

export const plantsModuleFetchRoomsListActionCreator = (
  token: string | null,
  notify: (text: string, type: NotificationType) => void
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_ROOMS_LIST_ACTION,
  payload: { token, notify },
});
export const plantsModuleFetchRoomsListSuccessActionCreator = (
  rooms: Array<Room>
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_ROOMS_LIST_SUCCESS_ACTION,
  payload: { rooms },
});
export const plantsModuleFetchRoomsListFailureActionCreator = (
  error: string
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_ROOMS_LIST_FAILURE_ACTION,
  payload: { error },
});
export const plantsModuleFetchSelectedRoomDataActionCreator = (
  roomId: number,
  token: string | null,
  dataDuration: PlantsModuleDataDuration,
  notify: (text: string, type: NotificationType) => void
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_ACTION,
  payload: { roomId, token, dataDuration, notify },
});
export const plantsModuleFetchSelectedRoomDataSuccessActionCreator = (
  roomData: RoomData | null
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_SUCCESS_ACTION,
  payload: { roomData },
});
export const plantsModuleFetchSelectedRoomDataFailureActionCreator = (
  error: string
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_SELECTED_ROOM_DATA_FAILURE_ACTION,
  payload: { error },
});
export const plantsModuleEditPlantDataActionCreator = (
  data: PlantData
): PlantsModuleAction => ({
  type: PLANTS_MODULE_EDIT_PLANT_DATA_ACTION,
  payload: { data },
});
export const plantsModuleFetchWateringModesActionCreator = (
  token: string | null
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_WATERING_MODES_ACTION,
  payload: { token },
});
export const plantsModuleFetchWateringModesSuccessActionCreator = (
  data: Array<WateringMode>
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_WATERING_MODES_SUCCESS_ACTION,
  payload: { data },
});
export const plantsModuleFetchWateringModesFailureActionCreator = (
  error: string
): PlantsModuleAction => ({
  type: PLANTS_MODULE_FETCH_WATERING_MODES_FAILURE_ACTION,
  payload: { error },
});
export const plantsModuleSetDataDurationActionCreator = (
  dataDuration: PlantsModuleDataDuration
): PlantsModuleAction => ({
  type: PLANTS_MODULE_SET_DATA_DURATION_ACTION,
  payload: { dataDuration },
});
export const plantsModuleUpdateRoomValuesActionCreator = (
  data: RoomDataApi
): PlantsModuleAction => ({
  type: PLANTS_MODULE_UPDATE_ROOM_VALUES_ACTION,
  payload: { data },
});
export const plantsModuleUpdatePlantValuesActionCreator = (
  data: PlantDataApi,
  plantId: number
): PlantsModuleAction => ({
  type: PLANTS_MODULE_UPDATE_PLANT_VALUES_ACTION,
  payload: { data, plantId },
});
