import isEmpty from "lodash/isEmpty";
import { Dispatch } from "react";
import { AnyAction, MiddlewareAPI } from "redux";

import { StoreState } from "../types/StoreState";
import {
  PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION,
  PlantsModuleAction,
  plantsModuleFetchWateringModesActionCreator,
} from "./modules/plantsModule/actions";
import { UserAction } from "./modules/user/actions";

type Middleware<E extends AnyAction> = (
  api: MiddlewareAPI
) => (next: Dispatch<E>) => (action: E) => ReturnType<Dispatch<E>>;

export const fetchStaticDataMiddleware: Middleware<
  UserAction | PlantsModuleAction
> = (api) => (next) => (action) => {
  const { plantsModule, user }: StoreState = api.getState();

  if (action.type === PLANTS_MODULE_SET_SELECTED_ROOM_SUCCESS_ACTION) {
    if (isEmpty(plantsModule.wateringModes)) {
      api.dispatch(
        plantsModuleFetchWateringModesActionCreator(user?.user?.token)
      );
    }
  }
  return next(action);
};
