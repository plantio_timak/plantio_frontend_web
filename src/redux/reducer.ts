import { AnyAction, CombinedState, combineReducers } from "redux";

import user, { initialState as userInitialState } from "./modules/user/reducer";
import plantsModule, {
  initialState as plantsModuleInitialState,
} from "./modules/plantsModule/reducer";
import { StoreState } from "../types/StoreState";
import { USER_LOGOUT_ACTION } from "./modules/user/actions";

const appReducer = combineReducers<StoreState>({
  user,
  plantsModule,
});

const initialState = {
  user: userInitialState,
  plantsModule: plantsModuleInitialState,
};

const rootReducer = (
  state: StoreState,
  action: AnyAction
): CombinedState<StoreState> => {
  if (action.type === USER_LOGOUT_ACTION) {
    return initialState;
  }

  return appReducer(state, action);
};

export default rootReducer;
