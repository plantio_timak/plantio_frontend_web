import { all } from "redux-saga/effects";

import userSaga from "./modules/user/sagas";
import plantsModuleSaga from "./modules/plantsModule/sagas";

export default function* rootSaga(): Generator {
  yield all([...userSaga, ...plantsModuleSaga]);
}
